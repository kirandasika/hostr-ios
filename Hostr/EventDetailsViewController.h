//
//  EventDetailsViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/5/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import <XLForm/XLForm.h>
#import <XLForm/XLFormViewController.h>
#import <INTULocationManager/INTULocationManager.h>
#import <AFNetworking/AFNetworking.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "EventNameViewController.h"

@interface EventDetailsViewController : XLFormViewController
@property (nonatomic, strong) Event *event;
@property (nonatomic) EventState eventState;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *createEventButton;
- (IBAction)validateAndContinue:(id)sender;

@end
