//
//  NotificationPermissions.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "NotificationPermissions.h"

@implementation NotificationPermissions
- (instancetype)init {
    self = [super init];
    if (self) {
        [self updateNotificationPermissions];
    }
    return self;
}

+ (NotificationPermissions *)sharedInstance {
    static NotificationPermissions *sharedInstance = nil;
    if (sharedInstance == nil) {
        static dispatch_once_t onceToken;
        _dispatch_once(&onceToken, ^{
            sharedInstance = [[NotificationPermissions alloc] init];
        });
    }
    
    return sharedInstance;
}

- (void)updateNotificationPermissions {
    self.hasNotificationsEnabled = [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
}


- (void)askNotificationsWithCompletionHandler:(void (^)(BOOL))completionHandler {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            self.hasNotificationsEnabled = granted;
            completionHandler(true);
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    completionHandler(true);
}

- (void) askNotifications {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}
@end
