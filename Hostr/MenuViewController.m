//
//  MenuViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/21/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "MenuViewController.h"
#import "ProfileTableViewCell.h"
#import "ProfileViewController.h"
#import "User.h"

@interface MenuViewController ()

@end

@implementation MenuViewController {
    NSArray *menuItems;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuItems = @[@"profile", @"find_events", @"my_events", @"chatrooms", @"logout"];
//    menuItems = @[@"profile", @"find_events", @"my_events", @"chatrooms", @"settings", @"share", @"contact_us", @"terms"];
    self.tableView.tableFooterView = [UIView new];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return menuItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 250.0f;
    }
    return 46.0f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIndentifier = [menuItems objectAtIndex:indexPath.row];
    
    if ([cellIndentifier isEqualToString:@"profile"]) {
        ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
        [cell configureData];
        return cell;
    }
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
    if (indexPath.row > 0) {
        cell.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, cell.bounds.size.width);
    }
    return cell;
}

- (BOOL) prefersStatusBarHidden {
    return YES;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"showProfileView"]) {
//        ProfileViewController *viewController = (ProfileViewController *)segue.destinationViewController;
//        viewController.profileState = ProfileStateCurrentUser;
//    }
    if ([segue.identifier isEqualToString:@"showLogout"]) {
        [[User sharedInstance] logout];
    }
}


@end
