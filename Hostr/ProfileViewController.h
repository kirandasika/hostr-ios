//
//  ProfileViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger, ProfileState) {
    ProfileStateCurrentUser,
    ProfileStateOtherUser
};

typedef NS_ENUM(NSUInteger, SenderSegueClass) {
    SenderSegueClassDefault,
    SenderSegueClassEventFeedViewController,
    SenderSegueClassEventViewController
};
@interface ProfileViewController : UITableViewController<UIScrollViewDelegate>
@property (nonatomic) ProfileState profileState;
@property (nonatomic) SenderSegueClass segueClass;
@property (nonatomic, strong) NSDictionary *tempUserDict;
@end
