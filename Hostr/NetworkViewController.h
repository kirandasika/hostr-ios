//
//  NetworkViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 9/18/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>

@interface NetworkViewController : UITableViewController
@property (nonatomic, strong) NSArray *followers;
@property (nonatomic, strong) NSArray *following;
@property (nonatomic, strong) NSArray *dataSourceArray;
@end
