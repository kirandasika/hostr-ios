//
//  User.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/22/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "User.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Event.h"

@implementation User

-(instancetype)initWithPropertyList:(NSString *)pListPath {
    self = [super init];
    if (self) {
        NSMutableDictionary *savedData = [NSMutableDictionary dictionaryWithContentsOfFile:pListPath];
        if ([savedData count] != 0) {
            userId = savedData[@"userId"];
            fbId = savedData[@"fbId"];
            fullName = savedData[@"fullname"];
            username = savedData[@"username"];
            email = savedData[@"email"];
            //birthday = savedData[@"birthday"];
            profilePictureURL = savedData[@"profilePictureURL"];
            fbAuthToken = savedData[@"fbAuthToken"];
            description = savedData[@"description"];
            profileLink = savedData[@"profileLink"];
            phoneNumber = savedData[@"phoneNumber"];
            score = savedData[@"score"] != nil ? [savedData[@"score"] integerValue] : 0;
            active = true;
        }
        else {
            active = false;
        }
    }
    return self;
}

+(User *)sharedInstance {
    static User *sharedInstance = nil;
    if (sharedInstance == nil) {
        static dispatch_once_t once;
        _dispatch_once(&once, ^{
            sharedInstance = [[[self class] alloc] initWithPropertyList:[User returnUserDataFilePath]];
        });
    }
    
    return sharedInstance;
}

#pragma mark - Getters

-(NSString *) userId {
    return userId;
}
-(NSString *) fbId {
    return fbId;
}
-(NSString *) fullName {
    return fullName;
}
-(NSString *) username {
    return username;
}
-(NSString *) email {
    return email;
}
//-(NSString *) birthday {
//    return birthday;
//}
- (NSString *)profilePictureURL {
    return profilePictureURL;
}
-(NSString *) fbAuthToken {
    return fbAuthToken;
}
- (NSString *)description {
    return description;
}
- (NSString *)profileLink {
    return profileLink;
}
- (NSString *)phoneNumber {
    return phoneNumber;
}
-(NSInteger) score {
    return score;
}
-(BOOL) isActive {
    return active;
}

#pragma mark - Methods

- (BOOL)saveData {
    BOOL flag = false;
    NSMutableDictionary *data = [NSMutableDictionary dictionaryWithContentsOfFile:[User returnUserDataFilePath]];
    [data setObject:userId forKey:@"userId"];
    [data setObject:fbId forKey:@"fbId"];
    [data setObject:fullName forKey:@"fullname"];
    [data setObject:username forKey:@"username"];
    [data setObject:email forKey:@"email"];
    //[data setObject:birthday forKey:@"birthday"];
    [data setObject:profilePictureURL forKey:@"profilePictureURL"];
    [data setObject:fbAuthToken forKey:@"fbAuthToken"];
    [data setObject:[NSString stringWithFormat:@"%ld", score] forKey:@"score"];
    [data setObject:(description == nil) ? @"" : description forKey:@"description"];
    [data setObject:(profileLink == nil) ? @"" : profileLink forKey:@"profileLink"];
    [data setObject:(phoneNumber == nil) ? @"" : phoneNumber forKey:@"phoneNumber"];
    
    
    flag = [data writeToFile:[User returnUserDataFilePath] atomically:YES];
    
    return flag;
}

-(void)registerUserInBackendWithData:(id)fbData andCompletionHandler:(void(^)(BOOL success, id result))completionHandler {
    //Open a connection to the backend and register the user
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSLog(@"%@", fbData);
    NSString *signUpURL = [[URLBuilder sharedInstance] urlWithEndpoint:@"users/signup/"];
    [manager POST:signUpURL parameters:fbData progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        userId = responseObject[@"pk"];
        responseObject = responseObject[@"fields"];
        fbId = fbData[@"id"];
        fullName = responseObject[@"full_name"];
        username = responseObject[@"username"];
        email = responseObject[@"email"];
       //birthday = responseObject[@"birthday"];
        profilePictureURL = responseObject[@"profile_picture_url"];
        fbAuthToken = responseObject[@"fb_access_token"];
        score = [responseObject[@"score"] integerValue];
        description = (responseObject[@"description"] != [NSNull null]) ? responseObject[@"description"] : nil;
        profileLink = (responseObject[@"profile_link"] != [NSNull null]) ? responseObject[@"profile_link"] : nil;
        phoneNumber = (responseObject[@"phone_number"] != [NSNull null]) ? responseObject[@"phone_number"] : nil;
        
        [self saveData];
        
        completionHandler(true, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error.localizedDescription);
        completionHandler(false, nil);
    }];
}


+ (void)getInformationFromFacebook {
    __block NSMutableDictionary *userSignUpData;
    NSDictionary *fields = @{@"fields": @"id,name,birthday,email,gender"};
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:fields] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (result){
            userSignUpData = [[NSMutableDictionary alloc] initWithDictionary:result];
            FBSDKGraphRequest *userFriendsRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/picture?type=large&redirect=false" parameters:nil HTTPMethod:@"GET"];
            [userFriendsRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                [userSignUpData setObject:result[@"data"][@"url"] forKey:@"profile_picture_url"];
                [userSignUpData setObject:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"fb_access_token"];
                //Registering the user with the backend
                [[User sharedInstance] registerUserInBackendWithData:userSignUpData andCompletionHandler:^(BOOL success, id result) {
                    if (success) {
                        NSLog(@"Signup success");
                    }
                    else {
                        NSLog(@"Signup failed trying again.");
                    }
                }];
            }];
        }
    }];
}

- (void)updateProfileInformationWithDictionary:(NSDictionary *)profileInformation {
    NSMutableDictionary *updatePayload = [[NSMutableDictionary alloc] init];
    if ([[profileInformation allKeys] containsObject:@"full_name"] && profileInformation[@"full_name"] != [NSNull null]) {
        [updatePayload setObject:profileInformation[@"full_name"] forKey:@"full_name"];
    }
    else {
        [updatePayload setObject:fullName forKey:@"full_name"];
    }
    if ([[profileInformation allKeys] containsObject:@"description"] && profileInformation[@"description"] != [NSNull null]) {
        [updatePayload setObject:profileInformation[@"description"] forKey:@"description"];
    }
    else {
        [updatePayload setObject:(description == nil || profileInformation[@"description"] == [NSNull null]) ? @"" : description forKey:@"description"];
    }
    
    if ([[profileInformation allKeys] containsObject:@"phone_number"] && profileInformation[@"phone_number"] != [NSNull null]) {
        [updatePayload setObject:profileInformation[@"phone_number"] forKey:@"phone_number"];
    }
    else {
        [updatePayload setObject:(phoneNumber == nil) ? @"" : phoneNumber forKey:@"phone_number"];
    }
    
    if ([[profileInformation allKeys] containsObject:@"profile_link"] && profileInformation[@"profile_link"] != [NSNull null]) {
        [updatePayload setObject:profileInformation[@"profile_link"] forKey:@"profile_link"];
    }
    else {
        [updatePayload setObject:(profileLink == nil) ? @"" : profileLink forKey:@"profile_link"];
    }
    
    if ([[profileInformation allKeys] containsObject:@"profile_picture"] && profileInformation[@"profile_picture"] != [NSNull null]) {
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *currentProfilePictureStorageRef = [[[storage reference] child:@"profile_pictures"] child:[NSString stringWithFormat:@"%@.jpeg", [self getUniqueHash]]];
        NSData *imageData = UIImageJPEGRepresentation(profileInformation[@"profile_picture"], 1.0f);
        [currentProfilePictureStorageRef putData:imageData metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
            if (!error) {
                profilePictureURL = metadata.downloadURL.absoluteString;
                
                // Updating data to server after uploading picture to storage bucket
                [updatePayload setObject:username forKey:@"username"];
                [updatePayload setObject:email forKey:@"email"];
                [updatePayload setObject:userId forKey:@"user_id"];
                [updatePayload setObject:(profilePictureURL != nil) ? profilePictureURL : @"" forKey:@"profile_picture_url"];
                
                
                //Upload data to server
                AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                NSString *updateURL = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/users/update_profile/"];
                [manager POST:updateURL parameters:updatePayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    if ([responseObject[@"response"] boolValue] == true) {
                        //Saved correctly
                        fullName = profileInformation[@"full_name"];
                        description = (profileInformation[@"description"] != [NSNull null]) ? profileInformation[@"description"] : @"";
                        profileLink = (profileInformation[@"profile_link"] != [NSNull null]) ? profileInformation[@"profile_link"] : @"";
                        phoneNumber = (profileInformation[@"phone_number"] != [NSNull null]) ? profileInformation[@"phone_number"] : @"";
                        [self saveData];
                    }
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"%@", task.response);
                }];
            }
        }];
        
        
    }
}


- (void)updateFCMTokenWithToken:(NSString *)fcmToken {
    NSDictionary *requestPayload = @{@"id": userId,
                                     @"device_token": fcmToken
                                     };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *fcmTokenUpdateEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/users/fcm_token/"];
    [manager POST:fcmTokenUpdateEndpoint parameters:requestPayload progress:nil success:nil failure:nil];
}


- (void)followUserWithId:(NSString *)userIdIn andWithCompletionHandler:(void (^)(BOOL))completionHandler {
    if (userIdIn != nil) {
        NSDictionary *requestPayload = @{@"user_id": userId,
                                         @"friend_id": userIdIn};
        NSString *followEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/users/follow/"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager POST:followEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@", responseObject);
            completionHandler(true);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            completionHandler(false);
        }];
    }
}

- (void)isFollowerForUserId:(NSString *)userIdIn andCompletionHandler:(void(^)(BOOL flag))completionHandler {
    NSDictionary *requestPayload = @{@"user_id": userId,
                                     @"friend_id": userIdIn};
    NSString *isFollowerEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/users/is_follower/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:isFollowerEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(TRUE);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(FALSE);
    }];
}


+(NSString *) returnUserDataFilePath {
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"UserData.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    
    if (![fileManager fileExistsAtPath:path]) {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"UserData" ofType:@"plist"];
        
        [fileManager copyItemAtPath:bundle toPath:path error:&error];
    }
    return path;
}

+ (void)getUserDataWithUserId:(NSString *)userIdIn andCompletionHandler:(void(^)(BOOL success, NSDictionary *user))completionHandler {
    NSString *getUserDataEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/users/get_data/"];
    NSDictionary *parameters = @{@"id": userIdIn};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:getUserDataEndpoint parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *userDict = [NSDictionary dictionaryWithDictionary:responseObject];
        
        completionHandler(TRUE, userDict);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(FALSE, nil);
    }];
}

- (void)getScoreWithCompletionHandler:(void(^)(BOOL success))completionHandler {
    NSDictionary *payload = @{@"user_id": userId};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *getScoreEndpoint = [[URLBuilder alloc] urlWithRequestType:@"GET" andWithEndpoint:@"/users/get_score/"];
    [manager GET:getScoreEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        score = [responseObject[@"score"] integerValue];
        [self saveData];
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(false);
    }];
}

- (void)getMyEventsWithCompletionHandler:(void(^)(BOOL success, NSArray *events))completionHandler {
    NSDictionary *payload = @{@"id": userId};
    NSString *getMyEventsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/events/my_events/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:getMyEventsEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *events = [NSMutableArray array];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kEventDateTimeFormat];
        for (NSDictionary *tempDict in responseObject) {
            Event *event = [[Event alloc] init];
            event.eventId = tempDict[@"id"];
            event.eventName = tempDict[@"event_name"];
            event.hosterInfo = tempDict[@"hoster"];
            event.startTimeStamp = [formatter dateFromString:tempDict[@"from_timestamp"]];
            event.endTimeStamp = [formatter dateFromString:tempDict[@"to_timestamp"]];
            event.price = [tempDict[@"price"] doubleValue];
            event.currency = tempDict[@"currency"];
            event.address = tempDict[@"address"];
            event.attending = (NSInteger *)[tempDict[@"attending"] integerValue];
            event.latitude = [tempDict[@"latitude"] doubleValue];
            event.longitude = [tempDict[@"longitude"] doubleValue];
            event.createdAt = [formatter dateFromString:tempDict[@"created_at"]];
            event.imageURL = tempDict[@"event_image_url"];
            event.isPrivate = [tempDict[@"is_private"] boolValue];
            event.imageData = nil;
            [events addObject:event];
        }
        
        completionHandler(true, events);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", task.response);
        completionHandler(false, nil);
    }];
}

- (void)logout {
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [self saveData];
}
- (NSString *)getUniqueHash{
    return [[NSUUID UUID] UUIDString];
}
@end
