//
//  PrivateInfoViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 6/30/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "PrivateInfoViewController.h"
#import "User.h"

@interface PrivateInfoViewController ()

@end

@implementation PrivateInfoViewController {
    NSArray *privateInfoItems;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    privateInfoItems = @[@"email", @"username", @"fb", @"payment_information"];
    self.tableView.tableFooterView = [UIView new];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return privateInfoItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIndentifier = [privateInfoItems objectAtIndex:indexPath.row];
    if ([cellIndentifier isEqualToString:@"birthday"]) {
        return 0.0f;
    }
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIndentifier = [privateInfoItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
    
    
    if ([cellIndentifier isEqualToString:@"email"]) {
        cell.textLabel.text = [[User sharedInstance] email];
    }
    else if ([cellIndentifier isEqualToString:@"username"]) {
        cell.textLabel.text = [[User sharedInstance] username];
    }
    else if ([cellIndentifier isEqualToString:@"birthday"]) {
        //cell.textLabel.text = [[User sharedInstance] birthday];
        cell.hidden = YES;
    }
    else if ([cellIndentifier isEqualToString:@"fb"]) {
        cell.textLabel.text = [[User sharedInstance] fbId];
    }
    
    
    
    
    if (![cellIndentifier isEqualToString:@"payment_information"]) {
        cell.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, cell.bounds.size.width);
    }
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
