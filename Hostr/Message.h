//
//  Message.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/13/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol MessageDelegate <NSObject>
@optional
- (void) messageTextChaged;

@end
@interface Message : NSObject
@property (nonatomic, weak) id<MessageDelegate> delegate;
@property (nonatomic, strong) NSString *Id;
@property (nonatomic, strong) NSString *senderId;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSDate *timestamp;
@end
