//
//  NetworkViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 9/18/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "NetworkViewController.h"
#import "URLBuilder.h"

@interface NetworkViewController ()

@end

@implementation NetworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}


- (void)getNetwork {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *getNetworkEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/users/get_network/"];
}
@end
