//
//  ChatroomsViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/30/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Event.h"
#import "SWRevealViewController.h"

@interface ChatroomsViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *myChatroomControl;

- (IBAction)valueChanged:(id)sender;

@end
