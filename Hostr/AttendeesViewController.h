//
//  AttendeesViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/31/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface AttendeesViewController : UITableViewController<EventEntity>
@property (nonatomic, strong) Event *event;
@end
