//
//  URLBuilder.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/22/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "URLBuilder.h"

#ifdef DEBUG
#if TARGET_IPHONE_SIMULATOR
NSString *const kBaseURI = @"http://localhost:8000";
#elif TARGET_OS_IPHONE
NSString *const kBaseURI = @"https://hostrdev.appspot.com"; //Ngrok URL
#endif
#else
NSString * const kBaseURI = @"https://hostrdev.appspot.com";
#endif

//NSString * const kBaseURI = @"https://hostrdev.appspot.com";

@implementation URLBuilder
+(URLBuilder *) sharedInstance {
    static URLBuilder * sharedInstance = nil;
    if (sharedInstance == nil) {
        static dispatch_once_t once;
        _dispatch_once(&once, ^{
            sharedInstance = [[[self class] alloc] init];
            NSLog(@"%@", kBaseURI);
        });
    }
    
    return sharedInstance;
}

-(NSString *) urlWithEndpoint:(NSString *)endpoint {
    //parsing the endpoint string for any errors;
    NSString *finalURL = nil;
    unichar firstChar = [endpoint characterAtIndex:0];
    if (firstChar != '/') {
        endpoint = [NSString stringWithFormat:@"/%@", endpoint];
    }
    
    finalURL = [NSString stringWithFormat:@"%@%@", kBaseURI, endpoint];
    return finalURL;
}

-(NSString *) urlWithRequestType:(NSString *)requestType andWithEndpoint:(NSString *)endpoint {
    NSString *finalURL = nil;
    unichar firstChar = [endpoint characterAtIndex:0];
    if (firstChar != '/') {
        endpoint = [NSString stringWithFormat:@"/%@", endpoint];
    }
    unichar lastChar = [endpoint characterAtIndex:([endpoint length] - 1)];
    if ([requestType isEqualToString:@"POST"] && lastChar != '/') {
        endpoint = [NSString stringWithFormat:@"%@/", endpoint];
    }
    
    finalURL = [NSString stringWithFormat:@"%@%@", kBaseURI, endpoint];
    return finalURL;
}
@end
