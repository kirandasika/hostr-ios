//
//  EventsViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/21/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <SAMCache/SAMCache.h>
#import "EventsViewController.h"
#import "EventNameViewController.h"
#import "EventViewCell.h"
#import "EventFeedViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <MaterialComponents/MaterialButtons.h>
#import "NotificationPermissions.h"
#import "NotificationPermissionViewController.h"
#import "SearchResultsViewController.h"

@interface EventsViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButtonItem;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSTimer *searchTimer;
@property (nonatomic, strong) NSString *searchQuery;
@end

@implementation EventsViewController {
    NSArray <Event *> *eventsFeed;
    UIRefreshControl *refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self customSetup];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView reloadData];
    
    MDCFloatingButton *floatingButton = [[MDCFloatingButton alloc] initWithFrame:CGRectZero];
    [self.view addSubview:floatingButton];
    floatingButton.translatesAutoresizingMaskIntoConstraints = NO;
    [floatingButton setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:45.0f/255.0f blue:85.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [floatingButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [floatingButton.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-16.0].active = YES;
    [floatingButton.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant: -16.0].active = YES;
    [floatingButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant: -30.0].active = YES;
//    [floatingButton setTitle:@"+" forState:UIControlStateNormal];
//    [floatingButton setTitle:@"-" forState:UIControlStateSelected];
    [floatingButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
    [floatingButton addTarget:self action:@selector(didTap) forControlEvents:UIControlEventTouchUpInside];
    
    if ([FBSDKAccessToken currentAccessToken] && [[User sharedInstance] fbId] != nil) {
        if (![[NotificationPermissions sharedInstance] hasNotificationsEnabled]) {
            [self openNotificationView];
        }
    }
    
    
    // Search Text Field Customization
    self.searchTextField.delegate = self;
    self.searchTextField.layer.cornerRadius = 4.0f;
    [self.searchTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didTap {
    NSLog(@"didTap");
    [self performSegueWithIdentifier:@"showEventName" sender:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([FBSDKAccessToken currentAccessToken]) {
        NSLog(@"%@", [[User sharedInstance] fullName]);
        if ([[User sharedInstance] fbId] == nil) {
            //Looks like this is the first time.
            //Getting all information of user
            __block NSMutableDictionary *userSignUpData;
            NSDictionary *fields = @{@"fields": @"id,name,birthday,email,gender"};
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:fields] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                if (result){
                    userSignUpData = [[NSMutableDictionary alloc] initWithDictionary:result];
                    FBSDKGraphRequest *userFriendsRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me/picture?type=large&redirect=false" parameters:nil HTTPMethod:@"GET"];
                    [userFriendsRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                        [userSignUpData setObject:result[@"data"][@"url"] forKey:@"profile_picture_url"];
                        [userSignUpData setObject:[FBSDKAccessToken currentAccessToken].tokenString forKey:@"fb_access_token"];
                        //Registering the user with the backend
                        [[User sharedInstance] registerUserInBackendWithData:userSignUpData andCompletionHandler:^(BOOL success, id result) {
                            if (success) {
                                //[self askNotifications];
                                
                                NSString *fcmToken = [FIRMessaging messaging].FCMToken;
                                NSLog(@"FCM registration token: %@", fcmToken);
                                [[User sharedInstance] updateFCMTokenWithToken:fcmToken];
                                
                                if (![[NotificationPermissions sharedInstance] hasNotificationsEnabled]) {
                                    [self openNotificationView];
                                }
                                
                                
                                NSLog(@"Signup success");
                                [Event getEventFeedWithCompletionHandler:^(BOOL success, NSArray<Event *> *events) {
                                    eventsFeed = events;
                                    [self.tableView reloadData];
                                }];
                            }
                            else {
                                NSLog(@"Signup failed trying again.");
                            }
                        }];
                    }];
                }
            }];
            
        }
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        if ([userDefaults objectForKey:@"first_time_login"] != nil && ![[userDefaults objectForKey:@"first_time_login"] isEqualToString:@"true"]) {
            [Event getEventFeedWithCompletionHandler:^(BOOL success, NSArray<Event *> *events) {
                eventsFeed = events;
                [self.tableView reloadData];
            }];
        }
        else {
            [userDefaults setObject:@"false" forKey:@"first_time_login"];
        }
        
    }
    else
        [self revealLoginViewController];
    
    //Changing nav bar color
//    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:94.0f/255.0f green:123.0f/255.0f blue:155.0/255.0f alpha:1.0f];
    [self customSetup];
}

- (void)openNotificationView {
    NotificationPermissionViewController *notificationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"notification_permission"];
    [self.navigationController presentViewController:notificationVC animated:YES completion:nil];
}

- (void)refresh {
    [Event getEventFeedWithCompletionHandler:^(BOOL success, NSArray<Event *> *events) {
        eventsFeed = events;
        [self.tableView reloadData];
        [refreshControl endRefreshing];
    }];
}

-(void) revealLoginViewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *loginViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    [self.navigationController presentViewController:loginViewController animated:YES completion:nil];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem setTarget: self.revealViewController];
        [self.revealButtonItem setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName, nil]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackOpaque];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:255.0f/255.0f green:215.0f/255.0f blue:0.0/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.tableView.refreshControl = refreshControl;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}



#pragma mark - Table View Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [eventsFeed count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EventViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Event *event = [eventsFeed objectAtIndex:indexPath.row];
    cell.eventNameLabel.text = [NSString stringWithFormat:@"%@", event.eventName];
    cell.fullNameLabel.text = event.hosterInfo[@"fields"][@"full_name"];
    cell.locationLabel.text = event.address;
    SAMCache *cache = [SAMCache sharedCache];
    if (event.imageData == nil) {
        NSString *cacheKey = [NSString stringWithFormat:@"event_%@", event.eventId];
        if ([cache imageForKey:cacheKey] == nil) {
            NSURL *imageURL = [NSURL URLWithString:event.imageURL];
            NSURL *profileImageURL = [NSURL URLWithString:event.hosterInfo[@"fields"][@"profile_picture_url"]];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                NSData *profileImageData = [NSData dataWithContentsOfURL:profileImageURL];
                if (imageData != nil) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        cell.eventImageView.image = [UIImage imageWithData:imageData];
                        event.imageData = (event.imageData == nil) ? imageData : event.imageData;
                        cell.profileImageView.image = [UIImage imageWithData:profileImageData];
                        
                        [cache setImage:[UIImage imageWithData:imageData] forKey:cacheKey];
                        [cache setImage:[UIImage imageWithData:profileImageData] forKey:[NSString stringWithFormat:@"user_%@", event.hosterInfo[@"pk"]]];
                    });
                }
            });
        }
        else {
            cell.eventImageView.image = [cache imageForKey:cacheKey];
            cell.profileImageView.image = [cache imageForKey:[NSString stringWithFormat:@"user_%@", event.hosterInfo[@"pk"]]];
            event.imageData = (event.imageData == nil) ? UIImageJPEGRepresentation([cache imageForKey:cacheKey], 1.0f) : event.imageData;
        }
    }
    else {
        cell.eventImageView.image = [UIImage imageWithData:event.imageData];
        cell.profileImageView.image = [cache imageForKey:[NSString stringWithFormat:@"user_%@", event.hosterInfo[@"pk"]]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tappedEvent = [eventsFeed objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"showViewEvent" sender:nil];
}

- (IBAction)logout:(id)sender{
    [[User sharedInstance] logout];
    [self revealLoginViewController];
}

# pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showEnterEventName"]) {
        EventNameViewController *viewController = (EventNameViewController *)segue.destinationViewController;
        viewController.eventState = EventCreationState;
    }
    
    if ([segue.identifier isEqualToString:@"showViewEvent"]) {
        EventFeedViewController *viewController = (EventFeedViewController *)segue.destinationViewController;
        viewController.event = self.tappedEvent;
    }
    
    if ([segue.identifier isEqualToString:@"showSearchResults"]) {
        SearchResultsViewController *viewController = (SearchResultsViewController *)segue.destinationViewController;
        viewController.searchQuery = self.searchQuery;
    }
}

- (void) askNotifications {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}



# pragma mark - Search Text Field Methods

- (void)textFieldDidChange:(UITextField *)textField {
    if (self.searchTimer != nil) {
        [self.searchTimer invalidate];
        self.searchTimer = nil;
    }
    
    
    self.searchTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(searchForQuery:) userInfo:self.searchTextField.text repeats:NO];
}

- (void)searchForQuery:(NSTimer *)timer {
    NSString *searchQuery = (NSString *)timer.userInfo;
    
    NSLog(@"Search Query: %@", searchQuery);
    
    if ([searchQuery length] > 1) {
        self.searchQuery = searchQuery;
        [self.searchTextField endEditing:YES];
        [self performSegueWithIdentifier:@"showSearchResults" sender:self];
    }
}
@end
