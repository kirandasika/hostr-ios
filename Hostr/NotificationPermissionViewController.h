//
//  NotificationPermissionViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationPermissionViewController : UIViewController<UIApplicationDelegate>
@property (nonatomic, weak) IBOutlet UIButton *allowPushNotificationsButton;
@property (nonatomic, weak) IBOutlet UIButton *skipNotificationButton;

- (IBAction)allowPushNotifications:(id)sender;
- (IBAction)skipPushNotifications:(id)sender;
@end
