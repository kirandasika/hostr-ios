//
//  EventDetailsViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/5/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "LocationViewController.h"
#import "LocationPermissions.h"


CGFloat const kEventSuccessAlertDuration = 3.0f;
CGFloat const kEventImageCompressionRate = 1.0f;
NSString *const kUserTempLocationFound = @"user_temp_location_found";
NSString *const kUserTempLocationForUpdateFound = @"user_temp_location_update_found";

@interface CLLocationValueTransformer : NSValueTransformer

@end

@implementation CLLocationValueTransformer

+ (Class)transformedValueClass
{
    return [NSString class];
}

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(id)value {
    if (!value)
    return nil;
    
    NSDictionary *valueDict = (NSDictionary *)value;
    if ([[valueDict allKeys] count] > 0) {
        return value[@"label_text"];
    }
    return @"Tap to Select";
}

@end

@interface EventDetailsViewController ()

@end

@implementation EventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[LocationPermissions sharedInstance] hasWhileInUseLocationEnabled]) {
        [self userLocation];
    }
    else {
        if (![[LocationPermissions sharedInstance] hasDeniedLocation]) {
            [self showPermissionsView];
        }
        else {
            UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Location Service Denied" message:@"Please re-enable location services for Hostr in the Settings app" preferredStyle:UIAlertControllerStyleAlert];
            [alertView addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:nil completionHandler:nil];
            }]];
            [alertView addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [alertView dismissViewControllerAnimated:YES completion:nil];
            }]];
            [self presentViewController:alertView animated:YES completion:nil];
        }
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeForm:) name:kUserTempLocationFound object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initializeForm:) name:kUserTempLocationForUpdateFound object:nil];
    
    
    // Check if Event is being updated
    if (self.eventState == EventUpdateState) {
        // Initialize the form with the new details.
        self.navigationItem.rightBarButtonItem = nil;
        UIBarButtonItem *updateButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Update Event" style:UIBarButtonItemStyleDone target:self action:@selector(updateEvent)];
        self.navigationItem.rightBarButtonItem = updateButtonItem;
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLocation) name:kLocationPermissionDone object:nil];
}

- (void)initializeForm:(NSNotification *)notification {
    XLFormDescriptor * form;
    XLFormSectionDescriptor * section;
    XLFormRowDescriptor * row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"Add Event"];
    
    // First section
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Start Time"];
    [form addFormSection:section];
    // Starts
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"starts" rowType:XLFormRowDescriptorTypeDateTimeInline title:@"Starts"];
    if (self.eventState == EventCreationState) {
        row.value = [NSDate dateWithTimeIntervalSinceNow:60*60*24];
    }
    else if (self.eventState == EventUpdateState) {
        row.value = self.event.startTimeStamp;
    }
    [row.cellConfig setObject:[UIImage imageNamed:@"Calender"] forKey:@"imageView.image"];
    [section addFormRow:row];
    
    
    // Ends
    if (self.eventState == EventUpdateState) {
        row = [XLFormRowDescriptor formRowDescriptorWithTag:@"ends" rowType:XLFormRowDescriptorTypeDateTimeInline title:@"Ends"];
        row.value = self.event.endTimeStamp;
        [row.cellConfig setObject:[UIImage imageNamed:@"Calender"] forKey:@"imageView.image"];
        [section addFormRow:row];
    }

    // Second Section
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Pricing & Other"];
    section.footerTitle = @"All Private events can be viewed under 'My Events'";
    [form addFormSection:section];
    
    
    //Ticket price
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"price" rowType:XLFormRowDescriptorTypeDecimal title:@"Ticket Price"];
    if (self.eventState == EventUpdateState) {
        row.value = [NSString stringWithFormat:@"%.2f", self.event.price];
    }
    [row.cellConfig setObject:[UIImage imageNamed:@"Dollar"] forKey:@"imageView.image"];
    [section addFormRow:row];
    
    
//    // Description
//    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"description" rowType:XLFormRowDescriptorTypeText title:@"Description"];
//    [row.cellConfig setObject:[UIImage imageNamed:@"About-you"] forKey:@"imageView.image"];
//    [section addFormRow:row];
    
    //Select event location
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"map_selector" rowType:XLFormRowDescriptorTypeSelectorPush title:@"Location"];
    [row.cellConfig setObject:[UIImage imageNamed:@"House_Filled"] forKey:@"imageView.image"];
    row.action.viewControllerStoryboardId = @"enterEventLocationView";
    row.valueTransformer = [CLLocationValueTransformer class];
    if (self.eventState != EventUpdateState) {
        row.value = [notification userInfo];
    }
    else {
        row.value = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f", self.event.latitude],@"latitude",[NSString stringWithFormat:@"%f", self.event.longitude],@"longitude",@"Click to update", @"label_text", nil];
    }
    [section addFormRow:row];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"is_private" rowType:XLFormRowDescriptorTypeBooleanSwitch title:@"Private"];
    row.value = self.event.isPrivate ? @(YES) : @(NO);
    [row.cellConfig setObject:[UIImage imageNamed:@"Private"] forKey:@"imageView.image"];
    [section addFormRow:row];
    
    //Select event poster/image
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"image" rowType:XLFormRowDescriptorTypeImage title:@"Select Poster/Image"];
    if (self.eventState == EventUpdateState) {
        row.value = [UIImage imageWithData:self.event.imageData];
        row.disabled = [NSNumber numberWithBool:YES];
    }
    [section addFormRow:row];
    
    
    self.form = form;
}

- (void)formRowDescriptorValueHasChanged:(XLFormRowDescriptor *)formRow oldValue:(id)oldValue newValue:(id)newValue {
    [super formRowDescriptorValueHasChanged:formRow oldValue:oldValue newValue:newValue];
    
    if (self.eventState != EventUpdateState) {
        if ([formRow.tag isEqualToString:@"starts"]) {
            if ([self.form formRowWithTag:@"ends"] != nil) {
                [self.form removeFormRowWithTag:@"ends"];
            }
            XLFormSectionDescriptor *section = [XLFormSectionDescriptor formSectionWithTitle:@"End Time"];
            // Ends
            XLFormRowDescriptor *row = [XLFormRowDescriptor formRowDescriptorWithTag:@"ends" rowType:XLFormRowDescriptorTypeDateTimeInline title:@"Ends"];
            row.value = [(NSDate *)newValue dateByAddingTimeInterval:60*60*24];
            [row.cellConfig setObject:[UIImage imageNamed:@"Calender"] forKey:@"imageView.image"];
            row.disabled = [NSNumber numberWithBool:NO];
            [section addFormRow:row];
            [self.form addFormSection:section atIndex:1];
            
            if ([[self.form formSections] count] > 3) {
                [self.form removeFormSectionAtIndex:2];
            }
        }
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

- (IBAction)validateAndContinue:(id)sender {
    NSLog(@"%@", [self.form formValues]);
    NSLog(@"%@", self.form.formValues[@"starts"]);
    NSLog(@"%@", [self gmtDescriptionFromDate:self.form.formValues[@"starts"]]);
    self.event.startTimeStamp = self.form.formValues[@"starts"];
    if ([self.createEventButton isEnabled]) {
        [self.createEventButton setEnabled:NO];
    }
    NSLog(@"%@", self.event.startTimeStamp);
    if (self.form.formValues[@"price"] != [NSNull null] && self.form.formValues[@"starts"] != [NSNull null]
        && self.form.formValues[@"ends"] != [NSNull null] && self.form.formValues[@"map_seletor"] != [NSNull null] &&
        self.form.formValues[@"image"] != [NSNull null]){
        self.event.price = [self.form.formValues[@"price"] integerValue];
        self.event.startTimeStamp = self.form.formValues[@"starts"];
        self.event.endTimeStamp = self.form.formValues[@"ends"];
        self.event.currency = @"USD";
        self.event.address = self.form.formValues[@"map_selector"][@"address"];
        self.event.latitude = [self.form.formValues[@"map_selector"][@"latitude"] doubleValue];
        self.event.longitude = [self.form.formValues[@"map_selector"][@"longitude"] doubleValue];
        self.event.isPrivate = [self.form.formValues[@"is_private"] boolValue];
        self.event.createdAt = [NSDate dateWithTimeIntervalSinceNow:60*60*24];
        if (self.event.imageData == nil) {
            self.event.imageData = UIImageJPEGRepresentation(self.form.formValues[@"image"],
                                                             kEventImageCompressionRate);
        }
        NSLog(@"Created at: %@", self.event.createdAt);
        //Upload data to server
        [self.event uploadEventWithCompletionHandler:^(BOOL success) {
            if (success){
                AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Voila!🙈" andText:@"Your event is sucessfully created." andCancelButton:NO forAlertType:AlertSuccess];
                [alertView.logoView setImage:[UIImage imageNamed:@"checkMark.png"]];
                [alertView show];
                alertView.completionBlock = ^(AMSmoothAlertView *alertObj, UIButton *button) {
                    if (button == alertObj.defaultButton){
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                };
            }
            else{
                AMSmoothAlertView *alertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Whoops!😕" andText:@"Error while creating event. Please try again" andCancelButton:NO forAlertType:AlertFailure];
                [alertView show];
                [self.createEventButton setEnabled:YES];
            }
        }];
    }
    else{
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Whoops!🙈" message:@"Please enter all details to create an event." preferredStyle:UIAlertControllerStyleAlert];
        [alertView addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [self presentViewController:alertView animated:YES completion:nil];
        [self.createEventButton setEnabled:YES];
    }
}

- (NSString *)gmtDescriptionFromDate:(NSDate *)date {
    static NSDateFormatter *df;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        df = [[NSDateFormatter alloc] init];
        df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    });
    return [df stringFromDate:date];
}

- (void) userLocation {
    INTULocationManager *locManager = [INTULocationManager sharedInstance];
    [locManager requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock timeout:10.0 block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
        if (status == INTULocationStatusSuccess) {
            NSMutableDictionary *tempLocationDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f", currentLocation.coordinate.latitude],@"latitude",[NSString stringWithFormat:@"%f", currentLocation.coordinate.longitude],@"longitude",@"Tap to Select", @"label_text", nil];
            if (self.eventState == EventCreationState) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kUserTempLocationFound object:nil userInfo:tempLocationDict];
            }
            else if (self.eventState ==   EventUpdateState) {
                [tempLocationDict setObject:@YES forKey:@"isUpdateState"];
                [[NSNotificationCenter defaultCenter] postNotificationName:kUserTempLocationForUpdateFound object:nil userInfo:tempLocationDict];
            }
        }
        else if (status == INTULocationStatusTimedOut) {
            NSLog(@"Timed out location");
        }
    }];
}

- (void)updateEvent {
    NSLog(@"Update event:\n%@", self.formValues);
    self.event.price = [self.form.formValues[@"price"] integerValue];
    self.event.startTimeStamp = self.form.formValues[@"starts"];
    self.event.endTimeStamp = self.form.formValues[@"ends"];
    self.event.currency = @"USD";
    self.event.address = (self.form.formValues[@"map_selector"][@"address"] == nil) ? self.event.address : self.form.formValues[@"map_selector"][@"address"];
    self.event.latitude = [self.form.formValues[@"map_selector"][@"latitude"] doubleValue];
    self.event.longitude = [self.form.formValues[@"map_selector"][@"longitude"] doubleValue];
    self.event.isPrivate = [self.form.formValues[@"is_private"] boolValue];
    [self.event updateEventWithCompletionHandler:^(BOOL success) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
}



#pragma mark - Location Services
- (void)showPermissionsView {
    LocationViewController *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"location_permissions"];
    [self.navigationController presentViewController:locationVC animated:YES completion:nil];
}


#pragma mark - Navigation
@end
