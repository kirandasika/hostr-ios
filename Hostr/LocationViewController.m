//
//  LocationViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "LocationViewController.h"

@interface LocationViewController ()

@end

@implementation LocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.allowLocationButton.layer.cornerRadius = 3.0f;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
}

- (IBAction)requestLocationPermissions:(id)sender {
    if ([[LocationPermissions sharedInstance] hasLocationServicesEnabled]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (IBAction)skipLocationPermissions:(id)sender {
    [[LocationPermissions sharedInstance] setHasWhileInUseLocationEnabled:NO];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [[LocationPermissions sharedInstance] setHasWhileInUseLocationEnabled:YES];
            [self dismissViewControllerAnimated:YES completion:^{
                [[LocationPermissions sharedInstance] sendUpdateNotification];
            }];
            break;
        case kCLAuthorizationStatusDenied:
            [[LocationPermissions sharedInstance] setHasWhileInUseLocationEnabled:NO];
            [[LocationPermissions sharedInstance] setHasDeniedLocation:YES];
            [[LocationPermissions sharedInstance] updateLocationPermissions];
            [self dismissViewControllerAnimated:YES completion:^{
                [[LocationPermissions sharedInstance] sendUpdateNotification];
            }];
            break;
        default:
            break;
    }
}


- (BOOL)prefersStatusBarHidden {
    return NO;
}

@end
