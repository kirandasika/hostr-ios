//
//  SearchResultsViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 9/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGObjectMapper/MGObjectMapper.h>
#import <MGObjectMapper/MGDateTransformer.h>
#import "Event.h"
#import "User.h"

@interface SearchResultsViewController : UITableViewController <UISearchBarDelegate>
@property (nonatomic, strong) NSString *searchQuery;

// IBOutlets
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchSegment;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


// Actions
- (IBAction)valueChangedForSearchSegment:(id)sender;
- (IBAction)backButtonTapped:(id)sender;
@end


@interface  SearchEvent: Event <MGObjectMapperResource>
@end

@interface SearchUser : User <MGObjectMapperResource>
@property (nonatomic, strong) NSString *pk;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *profilePictureUrl;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *profileLink;
@property (nonatomic) NSInteger score;
@end
