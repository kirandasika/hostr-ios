//
//  NotificationPermissionViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "NotificationPermissionViewController.h"
#import "NotificationPermissions.h"

@interface NotificationPermissionViewController ()

@end

@implementation NotificationPermissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.allowPushNotificationsButton.layer.cornerRadius = 3.0f;
}


- (IBAction)allowPushNotifications:(id)sender {
    NSLog(@"Allow");
    [[NotificationPermissions sharedInstance] askNotificationsWithCompletionHandler:^(BOOL success) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}


- (IBAction)skipPushNotifications:(id)sender {
    NSLog(@"Skip");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
