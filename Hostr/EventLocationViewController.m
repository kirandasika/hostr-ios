//
//  EventLocationViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/5/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "EventLocationViewController.h"
@import Mapbox;
@import AFNetworking;

NSString *const kMapBoxAPIEndpoint = @"https://api.mapbox.com";


@protocol DraggableAnnotationProtocol <NSObject>

@required
- (void)annotationDragFinishedWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end

// MGLAnnotationView subclass
@interface DraggableAnnotationView : MGLAnnotationView
@property (nonatomic, weak) id<DraggableAnnotationProtocol> delegate;
@end
@implementation DraggableAnnotationView

- (instancetype)initWithReuseIdentifier:(nullable NSString *)reuseIdentifier size:(CGFloat)size {
    self = [self initWithReuseIdentifier:reuseIdentifier];
    if (self)
    {
        // `draggable` is a property of MGLAnnotationView, disabled by default.
        self.draggable = true;
        
        // This property prevents the annotation from changing size when the map is tilted.
        self.scalesWithViewingDistance = false;
        
        // Begin setting up the view.
        self.frame = CGRectMake(0, 0, size, size);
        
        //self.backgroundColor = [UIColor darkGrayColor];
        
//        // Use CALayer’s corner radius to turn this view into a circle.
//        self.layer.cornerRadius = size / 2;
//        self.layer.borderWidth = 1;
//        self.layer.borderColor = [UIColor whiteColor].CGColor;
//        self.layer.shadowColor = [UIColor blackColor].CGColor;
//        self.layer.shadowOpacity = 0.1;
        
        UIImageView *mapPin = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size, size)];
        mapPin.image = [UIImage imageNamed:@"Map-Icon"];
        [self addSubview:mapPin];
    }
    return self;
}

- (BOOL)hasDelegate {
    return self.delegate != nil;
}

- (void)setDragState:(MGLAnnotationViewDragState)dragState animated:(BOOL)animated {
    [super setDragState:dragState animated:animated];
    
    switch (dragState) {
        case MGLAnnotationViewDragStateStarting:
            printf("Starting");
            [self startDragging];
            break;
            
        case MGLAnnotationViewDragStateDragging:
            printf(".");
            break;
            
        case MGLAnnotationViewDragStateEnding:
        case MGLAnnotationViewDragStateCanceling:
            printf("Ending\n");
            [self endDragging];
            
            // Check and calling delegate method annotationDragFinishedWithCoordinate
            if ([self hasDelegate])
                [self.delegate annotationDragFinishedWithCoordinate:self.annotation.coordinate];
                
            break;
            
        case MGLAnnotationViewDragStateNone:
            return;
    }
}

// When the user interacts with an annotation, animate opacity and scale changes.
- (void)startDragging {
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:0 animations:^{
        self.layer.opacity = 0.8;
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
    } completion:nil];
}

- (void)endDragging {
    self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.5, 1.5);
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:0 animations:^{
        self.layer.opacity = 1;
        self.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    } completion:nil];
}

@end


//
// Example view controller
@interface EventLocationViewController () <MGLMapViewDelegate, DraggableAnnotationProtocol>
@property MGLMapView *mapView;
@property MGLPointAnnotation *pinAnnotation;
@property UILongPressGestureRecognizer *longPress;
@end
@implementation EventLocationViewController {
    NSMutableArray *searchItems;
}


@synthesize  rowDescriptor = _rowDescriptor;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect mapViewFrame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height/2);
    self.mapView = [[MGLMapView alloc] initWithFrame:mapViewFrame];
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.mapView.styleURL = [MGLStyle streetsStyleURLWithVersion:9];
    self.mapView.tintColor = [UIColor darkGrayColor];
    self.mapView.zoomLevel = 15;
    self.mapView.centerCoordinate = CLLocationCoordinate2DMake([self.rowDescriptor.value[@"latitude"] doubleValue], [self.rowDescriptor.value[@"longitude"] doubleValue]);
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    
    
    // Setting up long press for map
    self.longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [self.mapView addGestureRecognizer:self.longPress];
    
    // Specify coordinates for our annotations.
    MGLPointAnnotation *annotation = [[MGLPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake([self.rowDescriptor.value[@"latitude"] doubleValue], [self.rowDescriptor.value[@"longitude"] doubleValue]);
    annotation.title = @"Your location";
    annotation.subtitle = @"Drag this to your desired location";
    self.pinAnnotation = annotation;
    [self.mapView addAnnotation:annotation];
    [self.mapView selectAnnotation:self.pinAnnotation animated:YES];
    
    self.rowDescriptor.value = nil;
    
    // Load default address
    [self getAddressWithCoordinate:annotation.coordinate];
    
    
    
//    self.mapView = mapView;
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateBegan) {
        if (self.pinAnnotation != nil) {
            printf("removing\n");
            [self.mapView removeAnnotation:self.pinAnnotation];
            self.pinAnnotation = nil;
            self.pinAnnotation = [[MGLPointAnnotation alloc] init];
            CGPoint touchPoint = [gesture locationInView:self.mapView];
            CLLocationCoordinate2D tappedCoordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
            NSLog(@"%.5f, %.5f", tappedCoordinate.latitude, tappedCoordinate.longitude);
            self.pinAnnotation.coordinate = [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
            self.pinAnnotation.title = @"Selected Location";
            self.pinAnnotation.subtitle = @"Press and drag to reposition your event.";
            [self.mapView addAnnotation:self.pinAnnotation];
            [self getAddressWithCoordinate:self.pinAnnotation.coordinate];
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self resignFirstResponder];
}

#pragma mark - MGLMapViewDelegate methods

// This delegate method is where you tell the map to load a view for a specific annotation. To load a static MGLAnnotationImage, you would use `-mapView:imageForAnnotation:`.
- (MGLAnnotationView *)mapView:(MGLMapView *)mapView viewForAnnotation:(id <MGLAnnotation>)annotation {
    // This example is only concerned with point annotations.
    if (![annotation isKindOfClass:[MGLPointAnnotation class]]) {
        return nil;
    }
    
    // For better performance, always try to reuse existing annotations. To use multiple different annotation views, change the reuse identifier for each.
    DraggableAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"draggablePoint"];
    
    // If there’s no reusable annotation view available, initialize a new one.
    if (!annotationView) {
        annotationView = [[DraggableAnnotationView alloc] initWithReuseIdentifier:@"draggablePoint" size:50];
        annotationView.delegate = self;
    }
    
    return annotationView;
}

- (BOOL)mapView:(MGLMapView *)mapView annotationCanShowCallout:(id<MGLAnnotation>)annotation {
    return YES;
}

- (void)annotationDragFinishedWithCoordinate:(CLLocationCoordinate2D)coordinate {
    // Check if coordinate if available and get the correct address
    self.rowDescriptor.value = [NSMutableDictionary dictionary];
    [self.rowDescriptor.value setObject:[NSString stringWithFormat:@"%f", coordinate.latitude] forKey:@"latitude"];
    [self.rowDescriptor.value setObject:[NSString stringWithFormat:@"%f", coordinate.longitude] forKey:@"longitude"];
    
    
    NSLog(@"%@", self.rowDescriptor.value);
    
    [self getAddressWithCoordinate:coordinate];
}

- (void)getAddressWithCoordinate:(CLLocationCoordinate2D)coordinate {
    self.addressTextView.text = @"Loading...";
    
    // Getting the address from mapbox
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/vnd.geo+json"];
    NSString *urlEndpoint = [NSString stringWithFormat:@"%@/geocoding/v5/mapbox.places/%.7f,%.7f.json?access_token=%@",
                             kMapBoxAPIEndpoint, coordinate.longitude, coordinate.latitude, [[NSBundle mainBundle] objectForInfoDictionaryKey:@"MGLMapboxAccessToken"]];
    NSLog(@"urlEndpoint: %@", urlEndpoint);
    [manager GET:urlEndpoint parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        NSLog(@"%@", responseObject[@"features"][0][@"place_name"]);
        
        
        if (responseObject[@"features"][0] != nil && self.rowDescriptor.value != nil) {
            [self.rowDescriptor.value setObject:responseObject[@"features"][0][@"place_name"] forKey:@"address"];
        }
        else {
            self.rowDescriptor.value = [NSMutableDictionary dictionary];
            [self.rowDescriptor.value setObject:responseObject[@"features"][0][@"place_name"] forKey:@"address"];
            [self.rowDescriptor.value setObject:[NSString stringWithFormat:@"%f", coordinate.latitude] forKey:@"latitude"];
            [self.rowDescriptor.value setObject:[NSString stringWithFormat:@"%f", coordinate.longitude] forKey:@"longitude"];
        }
        
        
        NSLog(@"%@", self.rowDescriptor.value);
        
        self.addressTextView.text = self.rowDescriptor.value[@"address"];
        [self.rowDescriptor.value setObject:@"Selected" forKey:@"label_text"];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //NSLog(@"%@", task.response);
        NSLog(@"%@", [error localizedDescription]);
    }];
}
@end
