//
//  CheckListItem.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/27/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "CheckListItem.h"
#import "User.h"
#import "URLBuilder.h"
#import <AFNetworking/AFNetworking.h>

@implementation CheckListItem
- (instancetype) init {
    self = [super init];
    if (self) {
//        self.eventId = nil;
//        self.userId = nil;
//        self.itemName = nil;
//        self.isCompleted = false;
        self.createdAt = nil;
    }
    return self;
}

- (void)completeItem {
    if (self.userId != nil && self.itemId != nil) {
        bool completed = (!self.isCompleted) ? true : false;
        NSDictionary *requestPayload = @{@"item_id": self.itemId,
                                         @"user_id": (completed) ? [[User sharedInstance] userId] : [NSNull null],
                                         @"item_flag": [NSNumber numberWithBool:completed]};
        NSString *changeItemEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/checklist_item_state/"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [manager POST:changeItemEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([self delegateIsSet]) {
                [_delegate itemCompleted:true];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if ([self delegateIsSet]) {
                [_delegate itemCompleted:false];
            }
            NSLog(@"Error: %@", task.response);
        }];
    }
}

- (BOOL)delegateIsSet {
    return self.delegate != nil;
}


- (void)deleteItemWithCompletionHandler:(void(^)(BOOL success))completionHandler {
    // Requesting server to delete checklist item
    NSDictionary *requestPayload = @{@"item_id": self.itemId};
    NSString *deleteItemEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/delete_item/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:deleteItemEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // Deletion successful
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // Deletion unsuccessful
        completionHandler(false);
    }];
}
@end
