//
//  URLBuilder.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/22/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface URLBuilder : NSObject
//CONSTANTS
extern NSString * const kBaseURI;

//Methods
+(URLBuilder *)sharedInstance;
-(NSString *) urlWithEndpoint:(NSString *)endpoint;
-(NSString *) urlWithRequestType:(NSString *)requestType andWithEndpoint:(NSString *)endpoint;
@end
