//
//  LocationPermissions.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "LocationPermissions.h"
#import "LocationViewController.h"

NSString *const kLocationPermissionDone = @"location_permission_done";

@implementation LocationPermissions
- (instancetype)init {
    self = [super init];
    if (self) {
        [self updateLocationPermissions];
    }
    return self;
}

+ (LocationPermissions *)sharedInstance {
    static LocationPermissions *sharedInstance = nil;
    if (sharedInstance == nil) {
        static dispatch_once_t onceToken;
        _dispatch_once(&onceToken, ^{
            sharedInstance = [[LocationPermissions alloc] init];
        });
    }
    return sharedInstance;
}

- (void)updateLocationPermissions {
    if ([CLLocationManager locationServicesEnabled]) {
        self.hasLocationServicesEnabled = YES;
    }
    else
        self.hasLocationServicesEnabled = NO;
    
    if (self.hasLocationServicesEnabled) {
        // Check if the user denied the location Services for the app
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
            self.hasWhileInUseLocationEnabled = NO;
            self.hasDeniedLocation = YES;
        }
        else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            self.hasWhileInUseLocationEnabled = NO;
        }
        else {
            self.hasWhileInUseLocationEnabled = YES;
        }
    
    }
}

- (void)sendUpdateNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:kLocationPermissionDone object:nil];
}
@end
