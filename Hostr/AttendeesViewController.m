//
//  AttendeesViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/31/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "AttendeesViewController.h"
#import "User.h"
#import "ProfileViewController.h"
#import <SAMCache/SAMCache.h>


@interface AttendeesViewController ()
@property (nonatomic, strong) NSArray<Attendee *> *attendees;
@end

@implementation AttendeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Load all the attendees
    [self.event loadAttendees];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.event.delegate = self;
    
    //Clearing the useless rows
    self.tableView.tableFooterView = [UIView new];
}

#pragma mark - Attendees Delegate Methods
- (void)loadedAttendees:(NSArray *)attendees {
    //Reload tableview
    self.attendees = attendees;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.attendees count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Attendee *attendee = [self.attendees objectAtIndex:indexPath.row];
    if ([attendee.gender isEqualToString:@"male"])
        cell.imageView.image = [UIImage imageNamed:@"User-male"];
    else
        cell.imageView.image = [UIImage imageNamed:@"User-female"];
    
    cell.textLabel.text = attendee.fullName;
    cell.detailTextLabel.text = @"";
    
    SAMCache *cache = [SAMCache sharedCache];
    NSString *userCacheKey = [NSString stringWithFormat:@"user_%@", attendee.userId];
    if (![cache imageExistsForKey:userCacheKey]) {
        NSURL *profilePictureUrl = [NSURL URLWithString:attendee.profilePictureURL];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSData *imageData = [NSData dataWithContentsOfURL:profilePictureUrl];
            dispatch_async(dispatch_get_main_queue(), ^{
                UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
                cell.imageView.image = [self imageWithImage:[UIImage imageWithData:imageData] convertToSize:CGSizeMake(80, 80)];
                //            cell.imageView.frame = CGRectMake(0, 0, 50, 50);
                [cell layoutSubviews];
                cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            });
        });
    }
    else {
        cell.imageView.image = [self imageWithImage:[cache imageForKey:userCacheKey] convertToSize:CGSizeMake(80, 80)];
    }
    
    return cell;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Attendee *attendee = [self.attendees objectAtIndex:indexPath.row];
    if ([attendee.userId integerValue] != [[[User sharedInstance] userId] integerValue]) {
        ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
        profileVC.profileState = ProfileStateOtherUser;
        profileVC.segueClass = SenderSegueClassEventFeedViewController;
        profileVC.tempUserDict = @{@"pk": attendee.userId,
                                   @"fields": @{@"full_name": attendee.fullName,
                                                @"profile_picture_url": attendee.profilePictureURL,
                                                @"description": attendee.desp,
                                                @"profile_link": attendee.profileLink,
                                                @"score": [NSString stringWithFormat:@"%ld", attendee.score]}
                                   };
        [self.navigationController pushViewController:profileVC animated:YES];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showProfileView"]) {
        
    }
}
@end
