//
//  EventsViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/21/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "SWRevealViewController.h"
#include "LoginViewController.h"
#import "User.h"
#import "URLBuilder.h"
#import "Event.h"

@interface EventsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (nonatomic, strong) Event *tappedEvent;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *logoutButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
- (IBAction)logout:(id)sender;
@end
