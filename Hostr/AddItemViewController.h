//
//  AddItemViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/16/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>
#import <XLForm/XLFormViewController.h>
#import "Event.h"
#import <AFNetworking/AFNetworking.h>

@interface AddItemViewController : XLFormViewController
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;
- (IBAction)doneAndPop:(id)sender;
@property (nonatomic, strong) Event *event;
@end
