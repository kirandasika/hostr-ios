//
//  CheckListItem.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/27/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CheckListItemDelegate <NSObject>

@required
- (void)itemCompleted:(bool)flag;

@end

@interface CheckListItem : NSObject
@property (nonatomic, strong) NSString *itemId;
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, assign) bool isCompleted;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, weak) id<CheckListItemDelegate> delegate;

- (instancetype)init;
- (void)completeItem;
- (void)deleteItemWithCompletionHandler:(void(^)(BOOL success))completionHandler;
@end
