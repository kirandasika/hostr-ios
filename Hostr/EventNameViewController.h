//
//  EventNameViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

typedef NS_ENUM(NSInteger, EventState) {
    EventCreationState,
    EventUpdateState
};
@interface EventNameViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *eventNameTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *continueButton;
@property (nonatomic) EventState eventState;
@property (nonatomic, strong) Event *event;
- (IBAction)eventDetails:(id)sender;
@end
