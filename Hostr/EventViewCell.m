//
//  EventViewCell.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "EventViewCell.h"

@implementation EventViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor colorWithRed:240.0f/255.0f green:240.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
    self.backgroundCardView.backgroundColor = [UIColor whiteColor];
    self.backgroundCardView.layer.cornerRadius = 3.0f;
    self.backgroundCardView.layer.masksToBounds = FALSE;
    self.backgroundCardView.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.2f].CGColor;
    self.backgroundCardView.layer.shadowOffset = CGSizeMake(1, 0);
    self.backgroundCardView.layer.shadowOpacity = 0.8f;
    self.backgroundCardView.layer.shadowRadius = 5.0f;
    
    
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
    self.profileImageView.clipsToBounds = YES;
    self.profileImageView.layer.borderWidth = 2.0f;
    self.profileImageView.layer.borderColor = [UIColor colorWithRed:253.0f/255.0f green:216.0f/255.0f blue:117.0f/255.0f alpha:1.0f].CGColor;
    //adding tap functionality
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToProfile)];
    tapGesture.numberOfTapsRequired = 1;
    [self.profileImageView addGestureRecognizer:tapGesture];
    [self.profileImageView setUserInteractionEnabled:YES];
}

- (void)goToProfile {
    NSLog(@"goToProfile");
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
