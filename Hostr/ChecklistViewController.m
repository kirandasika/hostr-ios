//
//  ChecklistViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/16/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "ChecklistViewController.h"
#import "AddItemViewController.h"

@implementation Item
- (instancetype) init {
    self = [super init];
    return self;
}
@end
@interface ChecklistViewController ()

@end

@implementation ChecklistViewController {
    NSArray *items;
    NSMutableDictionary *checkListDict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    checkListDict = [[NSMutableDictionary alloc] init];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getItemsFromServerWithCompletionHandler:^(BOOL success) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[checkListDict allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [checkListDict[@"Requested Items"] count];
    }
    else if (section == 1) {
        return [checkListDict[@"Completed"] count];
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70.0f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return [[checkListDict allKeys] objectAtIndex:section];
    }
    
    else if (section == 1) {
        return [[checkListDict allKeys] objectAtIndex:section];
    }
    
    return nil;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Item *item;
    if (indexPath.section == 0)
        item = [checkListDict[@"Requested Items"] objectAtIndex:indexPath.row];
    else if (indexPath.section == 1)
        item = [checkListDict[@"Completed"] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = item.itemName;
    if (indexPath.section == 1 && item.userId != nil) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"completed by %@", item.userId];
    }
    else {
        cell.detailTextLabel.text = @"";
    }
    return cell;
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 80)];
//    
//    UILabel *explanationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width - 20, 80)];
//    explanationLabel.textColor = [UIColor darkGrayColor];
//    explanationLabel.font = [UIFont fontWithName:@"Avenir Next" size:10.0f];
//    explanationLabel.numberOfLines = 0;
//    explanationLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    explanationLabel.text = @"Click on an item above to complete it 😁";
//    [footerView addSubview:explanationLabel];
//    
//    return footerView;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 50;
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckListItem *selectedItem;
    BOOL isCompleted;
    if (indexPath.section == 0) {
        selectedItem = [checkListDict[@"Requested Items"] objectAtIndex:indexPath.row];
        isCompleted = true;
    }
    else if (indexPath.section == 1) {
        selectedItem = [checkListDict[@"Completed"] objectAtIndex:indexPath.row];
        isCompleted = false;
    }
    [self showCompleteAlertControllerForItem:selectedItem andisCompleted:isCompleted];
}


- (void)showCompleteAlertControllerForItem:(CheckListItem *)item andisCompleted:(BOOL)isCompleted {
    UIAlertController *alertSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    if (isCompleted) {
        [alertSheet addAction:[UIAlertAction actionWithTitle:@"Mark as Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [item completeItem];
            [alertSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        [alertSheet addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [item deleteItemWithCompletionHandler:^(BOOL success) {
                if (success == false) {
                    NSLog(@"Deletion failed");
                }
            }];
            [checkListDict[@"Requested Items"] removeObject:item];
            [self.tableView reloadData];
            [alertSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
    }
    else {
        [alertSheet addAction:[UIAlertAction actionWithTitle:@"Mark as Undone" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [item completeItem];
            [alertSheet dismissViewControllerAnimated:YES completion:nil];
        }]];
        
    }
    
    [alertSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertSheet dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alertSheet animated:YES completion:nil];
    
}


- (IBAction)addItem:(id)sender {
    [self performSegueWithIdentifier:@"showAddItemView" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showAddItemView"]) {
        AddItemViewController *viewController = (AddItemViewController *)segue.destinationViewController;
        viewController.event = self.event;
    }
}


- (void) getItemsFromServerWithCompletionHandler:(void(^)(BOOL success))completionHandler {
    NSDictionary *requestPayload = @{@"event_id": self.event.eventId};
    NSString *getItemsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/get_items/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:getItemsEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *requestedItems = [[NSMutableArray alloc] init];
        NSMutableArray *completedItems = [[NSMutableArray alloc] init];
//        for (NSDictionary *tempDict in responseObject) {
//            Item *newItem = [[Item alloc] init];
//            newItem.itemId = tempDict[@"pk"];
//            newItem.eventId = tempDict[@"fields"][@"event"];
//            newItem.itemName = tempDict[@"fields"][@"item_name"];
//            newItem.userId = tempDict[@"fields"][@"user"];
//            newItem.isCompleted = [tempDict[@"fields"][@"item_flag"] boolValue];
//            if (!newItem.isCompleted)
//                [requestedItems addObject:newItem];
//            else
//                [completedItems addObject:newItem];
//        }
//        
//        if (completedItems.count == 0 && requestedItems.count != 0) {
//            // General test item for completed section
//            Item *testItem = [[Item alloc] init];
//            testItem.itemName = @"No items completed. Help out your host by completing an item on the checklist.";
//            [completedItems addObject:testItem];
//        }
        for (NSDictionary *tempDict in responseObject) {
            CheckListItem *newItem = [[CheckListItem alloc] init];
            newItem.itemId = tempDict[@"pk"];
            newItem.eventId = tempDict[@"fields"][@"event"];
            newItem.itemName = tempDict[@"fields"][@"item_name"];
            newItem.userId = tempDict[@"fields"][@"user"];
            newItem.isCompleted = [tempDict[@"fields"][@"item_flag"] boolValue];
            newItem.delegate = self;
            if (!newItem.isCompleted)
                [requestedItems addObject:newItem];
            else
                [completedItems addObject:newItem];
        }
        if (completedItems.count == 0 && requestedItems.count != 0) {
            // General test item for completed section
            Item *testItem = [[Item alloc] init];
            testItem.itemName = @"No items completed. Help out your host by completing an item on the checklist.";
            [completedItems addObject:testItem];
        }
        [checkListDict setObject:requestedItems forKey:@"Requested Items"];
        [checkListDict setObject:completedItems forKey:@"Completed"];
        [self.tableView reloadData];
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(false);
    }];
}


- (void) completeChecklistItem:(Item *)selectedItem {
    selectedItem.isCompleted = (selectedItem.isCompleted) ? NO : YES;
    NSDictionary *request = @{@"item_id": selectedItem.itemId,
                              @"user_id": [[User sharedInstance] userId],
                              @"item_flag": [NSString stringWithFormat:@"%d", selectedItem.isCompleted]};
    NSString *changeItemState = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/checklist_item_state/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:changeItemState parameters:request progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self getItemsFromServerWithCompletionHandler:^(BOOL success) {
            
        }];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

- (void)itemCompleted:(bool)flag {
    if (flag) {
        [self getItemsFromServerWithCompletionHandler:^(BOOL success) {
            
        }];
        NSLog(@"Done");
    }
}
@end
