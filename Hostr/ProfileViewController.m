//
//  ProfileViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "ProfileViewController.h"
#import "User.h"
#import "SWRevealViewController.h"

CGFloat const kTableHeaderHeight = 450.0f;
CGFloat const kTableHeaderCutAway = 60.0f;

@interface ProfileViewController ()
@property (nonatomic, weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButtonItem;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *userStatsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileCoverImageView;
-(IBAction)connect:(id)sender;
@end

@implementation ProfileViewController {
    NSArray *profileItems;
    UIView *headerView;
    CAShapeLayer *headerMaskLayer;
    BOOL privateInfoHidden;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customSetup];
    profileItems = @[@"private_info", @"description", @"profile_link"];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    headerView = self.tableView.tableHeaderView;
    self.tableView.tableHeaderView = nil;
    [self.tableView addSubview:headerView];
    
    self.tableView.contentInset = UIEdgeInsetsMake(kTableHeaderHeight, 0, 0, 0);
    self.tableView.contentOffset = CGPointMake(0, -(kTableHeaderHeight));
    self.tableView.tableFooterView = [UIView new];
    
    headerMaskLayer = [[CAShapeLayer alloc] init];
    headerMaskLayer.fillColor = [UIColor blackColor].CGColor;
    headerView.layer.mask= headerMaskLayer;
    
    [self updateHeaderView];
    
    
    // Hide the connect button if the its current user
    if (self.profileState == 0) {
        [self.connectButton setHidden:YES];
        [self.revealButtonItem setImage:[UIImage imageNamed:@"menu-1"]];
    }
    
    switch (self.segueClass) {
        case SenderSegueClassDefault:
            self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", [[User sharedInstance] score]];
            [self.connectButton setHidden:YES];
            break;
        case SenderSegueClassEventFeedViewController:
            self.scoreLabel.text = [NSString stringWithFormat:@"Score: %@", self.tempUserDict[@"fields"][@"score"]];
            self.editButtonItem.enabled = FALSE;
            self.navigationItem.rightBarButtonItem = nil;
            [self.revealButtonItem setTitle:@"Back"];
            privateInfoHidden = TRUE;
            break;
        case SenderSegueClassEventViewController:
            self.editButtonItem.enabled = FALSE;
            self.scoreLabel.text = [NSString stringWithFormat:@"Score: %@", self.tempUserDict[@"fields"][@"score"]];
            self.navigationItem.rightBarButtonItem = nil;
            [self.revealButtonItem setTitle:@"Back"];
            privateInfoHidden = TRUE;
        
        default:
            // control will never come reach default statement
            break;
    }
}


- (void)updateHeaderView {
    CGRect headerRect = CGRectMake(0, -(kTableHeaderHeight), self.tableView.bounds.size.width, kTableHeaderHeight);
    if (self.tableView.contentOffset.y < -kTableHeaderHeight) {
        headerRect.origin.y = self.tableView.contentOffset.y;
        headerRect.size.height = -self.tableView.contentOffset.y;
    }
    
    headerView.frame = headerRect;
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(headerRect.size.width, 0)];
    [path addLineToPoint:CGPointMake(headerRect.size.width, headerRect.size.height)];
    [path addLineToPoint:CGPointMake(0, headerRect.size.height - kTableHeaderCutAway)];
    headerMaskLayer.path = path.CGPath;
}
- (void)customSetup
{
    //Changing nav bar color
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:94.0f/255.0f green:123.0f/255.0f blue:155.0/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent = NO;
    
    
    if (self.profileState == ProfileStateCurrentUser) {
        SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            [self.revealButtonItem setTarget: self.revealViewController];
            [self.revealButtonItem setAction: @selector( revealToggle: )];
            [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
            [self.tableView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
            self.profileCoverImageView.userInteractionEnabled = YES;
            [self.tableView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
            [self.profileCoverImageView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        }
    }
    else if (self.profileState == ProfileStateOtherUser) {
        self.navigationItem.hidesBackButton = YES;
        UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(popToEventFeed)];
        self.navigationItem.leftBarButtonItem = backButtonItem;
        UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popToEventFeed)];
        rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
        headerView.userInteractionEnabled = YES;
        [headerView addGestureRecognizer:rightSwipe];
        [self.tableView addGestureRecognizer:rightSwipe];
    }
}

- (void)popToEventFeed {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)popToEventFeed:(UISwipeGestureRecognizer *)gesture {
    NSLog(@"backViewController");
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self updateHeaderView];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.connectButton setHidden:YES];
    [self setUpView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpView {
    self.userStatsLabel.text = @"Loading...";
    self.connectButton.layer.cornerRadius = 3.0f;
    self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.frame.size.width / 2;
    self.profilePictureImageView.layer.masksToBounds = NO;
    self.profilePictureImageView.layer.borderWidth = 2.0f;
    self.profilePictureImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.profilePictureImageView.clipsToBounds = YES;
    if (self.profileState == ProfileStateCurrentUser) {
        self.fullNameLabel.text = [[User sharedInstance] fullName];
        NSURL *profilePictureImageURL = [NSURL URLWithString:[[User sharedInstance] profilePictureURL]];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSData *imageData = [NSData dataWithContentsOfURL:profilePictureImageURL];
            if (imageData != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.profilePictureImageView.image = [UIImage imageWithData:imageData];
                });
            }
        });
    }
    else if (self.profileState == ProfileStateOtherUser){
        self.fullNameLabel.text = self.tempUserDict[@"fields"][@"full_name"];
        NSURL *profilePictureImageURL = [NSURL URLWithString:self.tempUserDict[@"fields"][@"profile_picture_url"]];
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(queue, ^{
            NSData *imageData = [NSData dataWithContentsOfURL:profilePictureImageURL];
            if (imageData != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.profilePictureImageView.image = [UIImage imageWithData:imageData];
                });
            }
        });
        // Checking if the user is a follower
        [[User sharedInstance] isFollowerForUserId:self.tempUserDict[@"pk"] andCompletionHandler:^(BOOL flag) {
            if (flag) {
                self.connectButton.enabled = FALSE;
                [self.connectButton setTitle:@"Connected" forState:UIControlStateDisabled];
            }
            else {
                self.connectButton.enabled = TRUE;
                [self.connectButton setTitle:@"Connect" forState:UIControlStateNormal];
            }
            self.connectButton.hidden = NO;
        }];
    }
    [self getStats];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return profileItems.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIndentifier = [profileItems objectAtIndex:indexPath.row];
    if ([cellIndentifier isEqualToString:@"private_info"] && privateInfoHidden)
        return 0.0f;
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIndentifier = [profileItems objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier forIndexPath:indexPath];
    
    if ([cellIndentifier isEqualToString:@"private_info"] && privateInfoHidden) {
        cell.hidden = YES;
    }
    
    if (self.profileState == ProfileStateCurrentUser) {
        if ([cellIndentifier isEqualToString:@"description"]) {
            if ([[[User sharedInstance] description] length] > 0) {
                cell.textLabel.text = [[User sharedInstance] description];
            }
            else {
                cell.textLabel.text = @"Description/bio";
                cell.textLabel.textColor = [UIColor lightGrayColor];
                cell.textLabel.font = [UIFont fontWithName:@"Avenir-Roman" size:13.0f];
            }
        }
        
        if ([cellIndentifier isEqualToString:@"profile_link"]) {
            if ([[[User sharedInstance] profileLink] length] > 0) {
                cell.textLabel.text = [[User sharedInstance] profileLink];
            }
            else {
                cell.textLabel.text = @"Social media link";
                cell.textLabel.textColor = [UIColor lightGrayColor];
                cell.textLabel.font = [UIFont fontWithName:@"Avenir-Roman" size:13.0f];
            }
        }

    }
    
    else if (self.profileState == ProfileStateOtherUser) {
        if ([cellIndentifier isEqualToString:@"description"]) {
            cell.textLabel.text = self.tempUserDict[@"fields"][@"description"];
        }
        
        if ([cellIndentifier isEqualToString:@"profile_link"]) {
            cell.textLabel.text = self.tempUserDict[@"fields"][@"profile_link"];
        }
    }
    
    if (indexPath.row > 0) {
        cell.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, cell.bounds.size.width);
    }
    return cell;
}

-(IBAction)connect:(id)sender {
    self.connectButton.enabled = NO;
  [[User sharedInstance] followUserWithId:self.tempUserDict[@"pk"] andWithCompletionHandler:^(BOOL success) {
      [self.connectButton setTitle:@"Connected" forState:UIControlStateDisabled];
  }];
}

- (void)getStats {
    NSString *userId;
    if (self.profileState == ProfileStateCurrentUser)
        userId = [[User sharedInstance] userId];
    else if (self.profileState == ProfileStateOtherUser)
        userId = self.tempUserDict[@"pk"];
    NSDictionary *payload = @{@"id": userId};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *userStatsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/users/stats/"];
    [manager GET:userStatsEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.userStatsLabel.text = [NSString stringWithFormat:@"Followers: %@ Following: %@", responseObject[@"followers"], responseObject[@"following"]];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.userStatsLabel.text = @"Failed to load";
    }];
}
@end
