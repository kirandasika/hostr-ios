//
//  NotificationPermissions.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

@interface NotificationPermissions : NSObject
@property (nonatomic, assign) BOOL hasNotificationsEnabled;


//Methods
+ (NotificationPermissions *)sharedInstance;
- (void)askNotificationsWithCompletionHandler:(void(^)(BOOL success))completionHandler;
@end
