//
//  ProfileTableViewCell.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 6/30/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "ProfileTableViewCell.h"
#import "User.h"

@implementation ProfileTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.frame.size.width / 2;
    self.profilePictureImageView.clipsToBounds = YES;
    self.profilePictureImageView.layer.borderWidth = 2.0f;
    self.profilePictureImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.userStatsLabel.text = @"Loading...";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) configureData {
    NSURL *profilePictureURI = [NSURL URLWithString:[[User sharedInstance] profilePictureURL]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:profilePictureURI];
        if (imageData != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.profilePictureImageView.image = [UIImage imageWithData:imageData];
            });
        }
    });
    
    self.fullNameLabel.text = [[User sharedInstance] fullName];
    self.scoreLabel.text = @"Loading...";
    [[User sharedInstance] getScoreWithCompletionHandler:^(BOOL success) {
        if (success) {
            self.scoreLabel.text = [NSString stringWithFormat:@"Score: %ld", [[User sharedInstance] score]];
        }
        else {
            self.scoreLabel.text = [NSString stringWithFormat:@"Failed to load"];
        }
    }];
    [self getStats];
}

- (void) getScore {
    NSDictionary *payload = @{@"user_id": [[User sharedInstance] userId]};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *userStatsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/users/get_score/"];
    [manager GET:userStatsEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.userStatsLabel.text = [NSString stringWithFormat:@"Followers: %@ Following: %@", responseObject[@"followers"], responseObject[@"following"]];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.userStatsLabel.text = @"Failed to load";
    }];
}

- (void)getStats {
    NSDictionary *payload = @{@"id": [[User sharedInstance] userId]};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *userStatsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/users/stats/"];
    [manager GET:userStatsEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.userStatsLabel.text = [NSString stringWithFormat:@"Followers: %@ Following: %@", responseObject[@"followers"], responseObject[@"following"]];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        self.userStatsLabel.text = @"Failed to load";
    }];
}

@end
