//
//  LocationPermissions.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface LocationPermissions : NSObject
@property (nonatomic, assign) BOOL hasLocationServicesEnabled;
@property (nonatomic, assign) BOOL hasWhileInUseLocationEnabled;
@property (nonatomic, assign) BOOL hasDeniedLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;

//Methods
+ (LocationPermissions *)sharedInstance;
- (void)sendUpdateNotification;
- (void)updateLocationPermissions;

//Constants
extern NSString *const kLocationPermissionDone;
@end
