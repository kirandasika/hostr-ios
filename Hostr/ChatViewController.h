//
//  ChatViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/13/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import "Event.h"
#import "User.h"
#import "AppDelegate.h"
#import "Message.h"
@import Firebase;

@interface ChatViewController : JSQMessagesViewController<MessageDelegate>
@property (nonatomic, strong) Event *event;
@property (nonatomic, strong) FIRDatabaseReference *ref;
- (IBAction)muteNotifications:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *muteNotificationsButton;

@end
