//
//  MyEventsViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/30/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "MyEventsViewController.h"
#import "EventFeedViewController.h"
#import <SAMCache/SAMCache.h>

@interface MyEventsViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *revealButtonItem;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSArray *events;
@property (nonatomic, strong) NSMutableArray *hostingEvents;
@property (nonatomic, strong) NSMutableArray *attendingEvents;
@property (nonatomic, strong) NSMutableArray *historyEvents;
@property (nonatomic, strong) Event *selectedEvent;
@end

@implementation MyEventsViewController{
    UIRefreshControl *refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getViewData];
    self.hostingEvents = [[NSMutableArray alloc] init];
    self.attendingEvents = [[NSMutableArray alloc] init];
    self.historyEvents = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self customSetup];
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem setTarget: self.revealViewController];
        [self.revealButtonItem setAction: @selector( revealToggle: )];
        [self.navigationController.navigationBar addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor], NSForegroundColorAttributeName, nil]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlackOpaque];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:255.0f/255.0f green:215.0f/255.0f blue:0.0/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent = NO;
    
    
    
    // Setting up refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(getViewData) forControlEvents:UIControlEventValueChanged];
    self.tableView.refreshControl = refreshControl;
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.events.count == 0) {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"😭";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        self.tableView.backgroundView = messageLabel;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
    self.tableView.backgroundView = nil;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.events count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Event *event = [self.events objectAtIndex:indexPath.row];
    cell.textLabel.text = event.eventName;
    cell.detailTextLabel.text = @"";
    
    // Checking if image exists in cache
    SAMCache *cache = [SAMCache sharedCache];
    NSString *cacheKey = [NSString stringWithFormat:@"event_%@", event.eventId];
    if (![cache imageExistsForKey:cacheKey]) {
    
    //Getting image from server
    NSURL *eventImageUrl = [NSURL URLWithString:event.imageURL];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:eventImageUrl];
        if (imageData != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [cache setImage:[UIImage imageWithData:imageData] forKey:cacheKey];
                event.imageData = imageData;
                cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
                cell.imageView.image = [self imageWithImage:[UIImage imageWithData:imageData] convertToSize:CGSizeMake(80, 80)];
                [cell layoutSubviews];
            });
        }
    });
    }
    else {
        cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
        cell.imageView.image = [self imageWithImage:[cache imageForKey:cacheKey] convertToSize:CGSizeMake(80, 80)];
        [cell layoutSubviews];
    }
    
    return cell;
}

- (void) getViewData {
    if ([self.activityIndicator isHidden])
        [self.activityIndicator setHidden:NO];
    [self.activityIndicator startAnimating];
    [[User sharedInstance] getMyEventsWithCompletionHandler:^(BOOL success, NSArray *events) {
        if (success && events != nil) {
            [self.hostingEvents removeAllObjects];
            [self.attendingEvents removeAllObjects];
            [self.historyEvents removeAllObjects];
            
            for (Event *event in events) {
                if ([[NSDate date] compare:event.startTimeStamp] == NSOrderedDescending) {
                    [self.historyEvents addObject:event];
                }
                else {
                    if ([[[User sharedInstance] userId] integerValue] == [event.hosterInfo[@"pk"] integerValue]) {
                        [self.hostingEvents addObject:event];
                    }
                    if ([[[User sharedInstance] userId] integerValue] != [event.hosterInfo[@"pk"] integerValue]) {
                        [self.attendingEvents addObject:event];
                    }
                }
            }
        }
        
        switch (self.myEventControl.selectedSegmentIndex) {
            case 0:
                self.events = self.hostingEvents;
                [self.tableView reloadData];
                break;
            case 1:
                self.events = self.attendingEvents;
                [self.tableView reloadData];
                break;
            case 2:
                self.events = self.historyEvents;
                [self.tableView reloadData];
                break;
            default:
                break;
        }
        [self.activityIndicator stopAnimating];
        [self.activityIndicator setHidden:YES];
        [refreshControl endRefreshing];
        
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedEvent = [self.events objectAtIndex:indexPath.row];
    if (self.selectedEvent != nil)
        [self performSegueWithIdentifier:@"showViewEvent" sender:self];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showViewEvent"]) {
        EventFeedViewController *viewController = (EventFeedViewController *)segue.destinationViewController;
        viewController.event = self.selectedEvent;
    }
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
- (IBAction)valueChanged:(id)sender {
    switch (self.myEventControl.selectedSegmentIndex) {
        case 0:
            self.events = self.hostingEvents;
            break;
        case 1:
            self.events = self.attendingEvents;
            break;
        case 2:
            self.events = self.historyEvents;
            break;
        default:
            
            break;
    }
    
    [self.tableView reloadData];
}
@end
