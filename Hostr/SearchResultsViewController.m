//
//  SearchResultsViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 9/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "SearchResultsViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "URLBuilder.h"
#import "Event.h"
#import "User.h"
#import "EventFeedViewController.h"
#import "ProfileViewController.h"

@implementation SearchEvent

+ (NSDictionary *)keyPathsByPropertyKey {
    return @{@"eventId": @"id",
             @"eventName": @"event_name",
             @"hosterInfo": @"hoster",
             @"price": @"price",
             @"currency": @"currency",
             @"address": @"address",
             @"attending": @"attending",
             @"latitude": @"latitude",
             @"longitude": @"longitude",
             @"imageURL": @"event_image_url",
             @"isPrivate": @"is_private",
             @"startTimeStamp": @"from_timestamp",
             @"endTimeStamp": @"to_timestamp",
             @"createdAt": @"created_at"};
}

+ (NSDictionary *)transformersByPropertyKey {
    return @{@"endTimeStamp": (NSDate *)[[MGDateTransformer alloc] init],
             @"createAt": (NSDate *)[[MGDateTransformer alloc] init]};
}

@end


@implementation SearchUser
+ (NSDictionary *)keyPathsByPropertyKey {
    return @{@"fullName": @"full_name",
             @"profilePictureUrl": @"profile_picture_url",
             @"description": @"description",
             @"profileLink": @"profile_link",
             @"score": @"score"};
}
@end

@interface SearchResultsViewController ()
@property (nonatomic, strong) NSMutableArray *dataSourceArray;
@property (nonatomic, strong) NSMutableArray *eventResults;
@property (nonatomic, strong) NSMutableArray *userResults;
@property (nonatomic, strong) SearchEvent *selectedEvent;
@end

@implementation SearchResultsViewController {
    AFHTTPSessionManager *_manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setting network manager settings in viewDidLoad
    _manager = [AFHTTPSessionManager manager];
    _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    self.dataSourceArray = [[NSMutableArray alloc] init];
    self.eventResults = [[NSMutableArray alloc] init];
    self.userResults = [[NSMutableArray alloc] init];
    
    self.searchBar.delegate = self;
    
    [self fetchResults];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.searchBar.text = self.searchQuery;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataSourceArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    if (self.searchSegment.selectedSegmentIndex == 0) {
        SearchEvent *event = [self.dataSourceArray objectAtIndex:indexPath.row];
        cell.textLabel.text = event.eventName;
    }
    
    
    if (self.searchSegment.selectedSegmentIndex == 1) {
        SearchUser *user = [self.dataSourceArray objectAtIndex:indexPath.row];
        cell.textLabel.text = user.fullName;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchSegment.selectedSegmentIndex == 0) {
        self.selectedEvent = [self.dataSourceArray objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"showSearchEvent" sender:self];
    }
    
    if (self.searchSegment.selectedSegmentIndex == 1) {
        SearchUser *attendee = [self.dataSourceArray objectAtIndex:indexPath.row];
        ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
        profileVC.profileState = ProfileStateOtherUser;
        profileVC.segueClass = SenderSegueClassEventFeedViewController;
        profileVC.tempUserDict = @{@"pk": attendee.pk,
                                   @"fields": @{@"full_name": attendee.fullName,
                                                @"profile_picture_url": attendee.profilePictureUrl,
                                                @"description": attendee.description,
                                                @"profile_link": attendee.profileLink,
                                                @"score": [NSString stringWithFormat:@"%ld", attendee.score]}
                                   };
        [self.navigationController pushViewController:profileVC animated:YES];
    }
}


# pragma mark - Network Requests
- (void)fetchResults {
    // Search query payload
    NSDictionary *requestPayload = @{@"search_query": self.searchQuery};
    
    // Fetches results for both events and users
    NSString *eventSearchEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/search/search_events/"];
    NSString *userSearchEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/search/search_users/"];
    
    // Sending network request for both endpoints
    
    // POST request for event search
    [_manager POST:eventSearchEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        
        // Checking for any returned results from the server
        
        // Delete any existing results that are being displayed
        if (self.eventResults.count > 0)
            [self.eventResults removeAllObjects];
        
        // Unserialize JSON data
        for (NSDictionary *tempDict in responseObject) {
            SearchEvent *event = [MGObjectMapper modelOfClass:SearchEvent.class fromDictionary:tempDict];
            event.startTimeStamp = [formatter dateFromString:tempDict[@"from_timestamp"]];
            event.endTimeStamp = [formatter dateFromString:tempDict[@"to_timestamp"]];
            [self.eventResults addObject:event];
        }
        
        [self.searchSegment setTitle:[NSString stringWithFormat:@"Events (%ld)", self.eventResults.count] forSegmentAtIndex:0];
        
        if (self.searchSegment.selectedSegmentIndex == 0 && self.eventResults.count > 0) {
            self.dataSourceArray = self.eventResults;
            [self.tableView reloadData];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // If the network request has failed display alert message for user
        
    }];
    
    
    // POST request for user search
    [_manager POST:userSearchEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        // Check for any returned results from the server
        
        
        //Delete any existing results that are being displayed
        if (self.userResults.count > 0)
            [self.userResults removeAllObjects];
        
        
        for (NSDictionary *tempDict in responseObject) {
            SearchUser *user = [MGObjectMapper modelOfClass:SearchUser.class fromDictionary:tempDict[@"fields"]];
            user.pk = tempDict[@"pk"];
            [self.userResults addObject:user];
        }
        
        
        [self.searchSegment setTitle:[NSString stringWithFormat:@"Users (%ld)", self.userResults.count] forSegmentAtIndex:1];
        
        if (self.searchSegment.selectedSegmentIndex == 1 && self.userResults.count > 0) {
            self.dataSourceArray = self.userResults;
            [self.tableView reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        // If network request fails display alert message for user
        
    }];
    
    
}



- (IBAction)valueChangedForSearchSegment:(id)sender {
    switch (self.searchSegment.selectedSegmentIndex) {
        case 0:
            self.dataSourceArray = self.eventResults;
            break;
        case 1:
            self.dataSourceArray = self.userResults;
            break;
        default:
            break;
    }
    
    [self.tableView reloadData];
}

- (IBAction)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - Search Bar

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchQuery = searchBar.text;
    [self fetchResults];
    [searchBar endEditing:YES];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showSearchEvent"]) {
        EventFeedViewController *viewController = (EventFeedViewController *)segue.destinationViewController;
        viewController.event = self.selectedEvent;
    }
}





@end
