//
//  DescriptionTableViewCell.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 6/30/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
