//
//  User.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/22/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "URLBuilder.h"
@import Firebase;

@interface User : NSObject {
    @private
    NSString *userId;
    NSString *fbId;
    NSString *fullName;
    NSString *username;
    NSString *email;
    //NSString *birthday;
    NSString *profilePictureURL;
    NSString *description;
    NSString *profileLink;
    NSString *fbAuthToken;
    NSString *phoneNumber;
    NSInteger score;
    BOOL active;
};


//Getters
-(NSString *) userId;
-(NSString *) fbId;
-(NSString *) fullName;
-(NSString *) username;
-(NSString *) email;
//-(NSString *) birthday;
-(NSString *) profilePictureURL;
-(NSString *) fbAuthToken;
-(NSString *) description;
-(NSString *) profileLink;
-(NSString *) phoneNumber;
-(NSInteger) score;
-(BOOL) isActive;

// Setters

//Methods
+ (User *)sharedInstance;
+ (void)getUserDataWithUserId:(NSString *)userIdIn andCompletionHandler:(void(^)(BOOL success, NSDictionary *user))completionHandler;
- (void)registerUserInBackendWithData:(id)fbData andCompletionHandler:(void(^)(BOOL success, id result))completionHandler;
+ (void)getInformationFromFacebook;
- (void)updateProfileInformationWithDictionary:(NSDictionary *)profileInformation;
- (void) updateFCMTokenWithToken:(NSString *)fcmToken;
- (void) followUserWithId:(NSString *)userIdIn andWithCompletionHandler:(void(^) (BOOL success))completionHandler;
- (void)isFollowerForUserId:(NSString *)userIdIn andCompletionHandler:(void(^)(BOOL flag))completionHandler;
- (void)getScoreWithCompletionHandler:(void(^)(BOOL success))completionHandler;
- (void)getMyEventsWithCompletionHandler:(void(^)(BOOL success, NSArray *events))completionHandler;
- (void)logout;
@end
