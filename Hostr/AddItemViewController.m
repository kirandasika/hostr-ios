//
//  AddItemViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/16/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "AddItemViewController.h"
#import "URLBuilder.h"

@interface AddItemViewController ()

@end

@implementation AddItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initializeForm];
}


- (void)initializeForm {
    XLFormDescriptor *form;
    XLFormSectionDescriptor *section;
    XLFormRowDescriptor *row;
    
    form = [XLFormDescriptor formDescriptorWithTitle:@"Add Item"];
    
    section = [XLFormSectionDescriptor formSection];
    
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"item_name" rowType:XLFormRowDescriptorTypeTextView title:@"Item Name"];
    
    [section addFormRow:row];
    
    [form addFormSection:section];
    
    self.form = form;
}
- (IBAction)doneAndPop:(id)sender {
    [self.doneButton setEnabled:NO];
    [self.doneButton setTitle:@"Updating..."];
    [self uploadItemToServerWithCompletionHandler:^(BOOL success) {
        if (success == true) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void) uploadItemToServerWithCompletionHandler:(void(^)(BOOL success))completionHandler {
    NSDictionary *requestPayload = @{@"event_id": self.event.eventId,
                                     @"item_name": self.formValues[@"item_name"]};
    NSString *newItemEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/new_checklist_item/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:newItemEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(false);
    }];
}
@end
