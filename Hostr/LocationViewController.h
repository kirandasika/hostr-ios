//
//  LocationViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationPermissions.h"
#import <CoreLocation/CoreLocation.h>

@interface LocationViewController : UIViewController<CLLocationManagerDelegate>
@property (nonatomic, weak) IBOutlet UIButton *allowLocationButton;
@property (nonatomic, weak) IBOutlet UIButton *skipButton;
@property (nonatomic, strong) CLLocationManager *locationManager;
//IBActions
- (IBAction)requestLocationPermissions:(id)sender;
- (IBAction)skipLocationPermissions:(id)sender;
@end
