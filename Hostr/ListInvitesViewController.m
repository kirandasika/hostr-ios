//
//  ListInvitesViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/4/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "ListInvitesViewController.h"

@interface ListInvitesViewController ()
@property (nonatomic, strong) NSArray *invitees;
@property (nonatomic, strong) NSMutableArray *selectedInvitees;
@end

@implementation ListInvitesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getInvitees];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.invitees count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSDictionary *user = [self.invitees objectAtIndex:indexPath.row];
    cell.textLabel.text = user[@"fields"][@"full_name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *user = [self.invitees objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (![self.selectedInvitees containsObject:user[@"pk"]]) {
            [self.selectedInvitees addObject:user[@"pk"]];
        }
        return;
    }
    
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.selectedInvitees removeObject:user[@"pk"]];
        if ([cell isSelected]) {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        return;
    }
}

-(IBAction)inviteConnections:(id)sender {
    NSLog(@"Invite: %@", self.selectedInvitees);
    NSDictionary *payload = @{@"event_id": self.event.eventId,
                              @"selected_ids": (self.selectedInvitees.count != 0 || self.selectedInvitees != nil) ? self.selectedInvitees : @[]};
    NSString *inviteConnectionsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/invite_connections/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:inviteConnectionsEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}



- (void)requestInvitees {
    // Alternative to the current invite connections so that more data in the notification sent.
    NSLog(@"Invitees: %@", self.selectedInvitees);
    NSDictionary *payload = @{@"event_id": self.event.eventId,
                              @"selected_ids": (self.selectedInvitees.count != 0 || self.selectedInvitees != nil) ? self.selectedInvitees : @[],
                              @"invite_sent_by": [[User sharedInstance] userId]};
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *inviteConnectionsEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/invite_connections/"];
    [manager POST:inviteConnectionsEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}


- (void)getInvitees {
    // Grab all invitees from the server
    NSDictionary *payload = @{@"event_id": self.event.eventId,
                              @"user_id": [[User sharedInstance] userId]};
    NSString *listInviteesEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/events/list_invitees/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:listInviteesEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *tempArr = [NSMutableArray array];
        for (NSDictionary *tempDict in responseObject) {
            [tempArr addObject:tempDict];
        }
        self.invitees = tempArr;
        self.selectedInvitees = [[NSMutableArray alloc] initWithCapacity:self.invitees.count];
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", [error localizedDescription]);
    }];
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}
@end
