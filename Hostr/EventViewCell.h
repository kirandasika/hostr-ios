//
//  EventViewCell.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *backgroundCardView;
@property (weak, nonatomic) IBOutlet UIImageView *eventImageView;
@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@end
