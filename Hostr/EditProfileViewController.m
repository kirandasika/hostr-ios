//
//  EditProfileViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/7/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeForm];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initializeForm {
    XLFormDescriptor *form;
    XLFormSectionDescriptor *section;
    XLFormRowDescriptor *row;
    
    form = [XLFormDescriptor formDescriptor];
    
    //Section 1
    section = [XLFormSectionDescriptor formSectionWithTitle:@"Edit Profile Information"];
    
    //Update profile picture
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"profile_picture" rowType:XLFormRowDescriptorTypeImage title:@"Change Profile Photo"];
    //Getting current profile picture
    NSURL *ppURL = [NSURL URLWithString:[[User sharedInstance] profilePictureURL]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:ppURL];
        if (imageData != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                row.value = [UIImage imageWithData:imageData];
                [row.cellConfig setObject:[UIImage imageWithData:imageData] forKey:@"imageView.image"];
                [section addFormRow:row];
            });
        }
        else{
            [section addFormRow:row];
        }
    });
    
    
    //Full name
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"full_name" rowType:XLFormRowDescriptorTypeText title:@""];
    row.value = [[User sharedInstance] fullName];
    [row.cellConfig setObject:[UIImage imageNamed:@"username"] forKey:@"imageView.image"];
    [section addFormRow:row];
    
    
    //Description
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"description" rowType:XLFormRowDescriptorTypeText title:@""];
    [row.cellConfig setObject:[UIImage imageNamed:@"desp"] forKey:@"imageView.image"];
    if ([[User sharedInstance] description] != nil) {
        row.value = [[User sharedInstance] description];
    }
    [section addFormRow:row];
    
    
    //Profile link
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"profile_link" rowType:XLFormRowDescriptorTypeURL title:@""];
    [row.cellConfig setObject:[UIImage imageNamed:@"link"] forKey:@"imageView.image"];
    if ([[User sharedInstance] profileLink]) {
        row.value = [[User sharedInstance] profileLink];
    }
    [section addFormRow:row];
    
    
    //Phone number
    row = [XLFormRowDescriptor formRowDescriptorWithTag:@"phone_number" rowType:XLFormRowDescriptorTypePhone title:@"Mobile Number"];
    row.value = ([[User sharedInstance] phoneNumber] != nil) ? [[User sharedInstance] phoneNumber] : nil;
    [section addFormRow:row];
    
    [form addFormSection:section];
    self.form = form;
}

- (IBAction)saveProfileInfo:(id)sender {
    if (self.form.formValues[@"full_name"] != [NSNull null]) {
        [[User sharedInstance] updateProfileInformationWithDictionary:self.form.formValues];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        AMSmoothAlertView *errorAlertView = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Snap!😬" andText:@"You need to enter a full name. A full name is required for others to find you." andCancelButton:NO forAlertType:AlertFailure];
        [errorAlertView show];
    }
}
@end
