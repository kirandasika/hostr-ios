//
//  LoginViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 5/21/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "LoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FirebaseCrash/FirebaseCrash.h>

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Handling clicks on loginButton
    [self.loginButton addTarget:self action:@selector(loginWithFacebook) forControlEvents:UIControlEventTouchUpInside];
    self.loginButton.layer.cornerRadius = 6.0f;
}


-(void)loginWithFacebook{
    FIRCrashLog(@"Signing up for facebook");
    NSArray *facebookPermissions = @[@"public_profile", @"email", @"user_friends", @"user_birthday"];
    FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
    [loginManager logInWithReadPermissions:facebookPermissions fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            NSLog(@"error in login");
        }
        else if (result.isCancelled) {
            NSLog(@"Cancelled");
        }
        else {
            NSLog(@"Logged in.");
            //Now register user with the server
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:@"true" forKey:@"first_time_login"];
            [userDefaults synchronize];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

@end
