//
//  EventFeedViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "EventFeedViewController.h"
#import "ChatViewController.h"
#import "ChecklistViewController.h"
#import "ProfileViewController.h"
#import "AttendeesViewController.h"
#import "EventNameViewController.h"
#import "ListInvitesViewController.h"
#import <SAMCache/SAMCache.h>

CGFloat const kEventTableHeaderHeight = 300.0f;
NSString * const kAttendStateChanged = @"attendStateChanged";

@implementation EventFeedAttendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.attendButton.layer.cornerRadius = 3.0f;
    self.infoLabel.text = @"All Payments are handled by Stripe Inc.";
    [self.attendButton addTarget:self action:@selector(processPaymentForEvent:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)processPaymentForEvent:(Event *)event {
    NSLog(@"Paid");
    self.attendButton.enabled = NO;
    if (!self.attending) {
        NSDictionary *requestPayload = @{@"event_id": self.event.eventId,
                                         @"user_id": [[User sharedInstance] userId]};
        NSString *attendEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/attend/"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager POST:attendEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.attendButton.titleLabel.text = @"Cancel";
            self.infoLabel.text = @"Clicking cancel will initiate a refund process.";
            self.attendButton.enabled = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kAttendStateChanged object:nil userInfo:nil];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
    }
    else {
        // Cancel the user from
        NSDictionary *requestPayload = @{@"event_id": self.event.eventId,
                                         @"user_id": [[User sharedInstance] userId]};
        NSString *unattendEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/cancel_attendee/"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager POST:unattendEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            self.attendButton.titleLabel.text = @"Attend";
            self.infoLabel.text = @"All payments are handles by Stripe Inc.";
            self.attendButton.enabled = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kAttendStateChanged object:nil userInfo:@{@"response": @"false"}];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
                                         
    }
}


@end



@interface EventFeedViewController ()

@end

@implementation EventFeedViewController {
    UIView *headerView;
    CAShapeLayer *headerMaskLayer;
    NSMutableDictionary<NSString *, NSArray *> *eventDetails;
    BOOL attending;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    headerView = self.tableView.tableHeaderView;
    self.tableView.tableHeaderView = nil;
    [self.tableView addSubview:headerView];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.tableView.contentInset = UIEdgeInsetsMake(kEventTableHeaderHeight, 0, 0, 0);
    self.tableView.contentOffset = CGPointMake(0, -(kEventTableHeaderHeight));
    self.tableView.tableFooterView = [UIView new];
    
    [self updateHeaderView];
    

    
    eventDetails = [[NSMutableDictionary alloc] init];
    [eventDetails setObject:@[@"hoster", @"date_time_field", @"ticket_price", @"address", @"attending", @"attend_button"] forKey:@"event_details"];
    //[eventDetails setObject:@[@"function_cells", @"function_cells"] forKey:@"Function"];
    //attending = true;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(attendStateChanged:) name:kAttendStateChanged object:nil];
    
    // Showing edit button if Host is viewing his event
    if ([self.event.hosterInfo[@"pk"] integerValue] == [[[User sharedInstance] userId] integerValue]) {
        // Show edit button
        UIBarButtonItem *editButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(showEventSettings)];
        self.navigationItem.rightBarButtonItem = editButtonItem;
    }
    
}

- (void)showEventSettings {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Settings" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Invite Connections" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        ListInvitesViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"list_invites"];
        viewController.event = self.event;
        [self.navigationController pushViewController:viewController animated:YES];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Update Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self performSegueWithIdentifier:@"showEventName" sender:self];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Delete Event" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        [self.event deleteEventWithCompletionHandler:^(BOOL success) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) attendStateChanged:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    if ([userInfo[@"response"] isEqualToString:@"false"]) {
        [self.event decreaseAttend];
        attending = false;
        [eventDetails removeObjectForKey:@"Functions"];
        [self.tableView reloadData];
    }
    else {
        [self.event increaseAttend];
        attending = true;
        if (eventDetails[@"Functions"] == nil) {
            [eventDetails setObject:@[@"function_cells", @"function_cells"] forKey:@"Functions"];
        }
        [self.tableView reloadData];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    SAMCache *cache = [SAMCache sharedCache];
    NSString *cacheKey = [NSString stringWithFormat:@"event_%@", self.event.eventId];
    if ([cache imageForKey:cacheKey] != nil) {
        self.headerImageView.image = [cache imageForKey:cacheKey];
    }
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    self.eventNameLabel.text = [NSString stringWithFormat:@"%@", self.event.eventName];
    [self.profilePictureImageView setUserInteractionEnabled:YES];
    self.profilePictureImageView.layer.cornerRadius = self.profilePictureImageView.frame.size.width / 2;
    self.profilePictureImageView.layer.borderWidth = 2.0f;
    self.profilePictureImageView.layer.borderColor = [UIColor colorWithRed:253.0f/255.0f green:216.0f/255.0f blue:117.0f/255.0f alpha:1.0f].CGColor;
    self.profilePictureImageView.clipsToBounds = YES;
    //Adding a gesture recognizer to a UIImageView (self.profilePictureImageView)
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:nil action:@selector(goToProfile:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.profilePictureImageView addGestureRecognizer:tapGesture];
    [self.placeHolderView bringSubviewToFront:self.profilePictureImageView];
    [self.headerImageView addGestureRecognizer:tapGesture];
    NSURL *profilePictureURL = [NSURL URLWithString:self.event.hosterInfo[@"fields"][@"profile_picture_url"]];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:profilePictureURL];
        if (imageData != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.profilePictureImageView.image = [UIImage imageWithData:imageData];
            });
        }
    });
    self.userNameLabel.text = [NSString stringWithFormat:@"by %@", self.event.hosterInfo[@"fields"][@"full_name"]];
    [self.userNameLabel addGestureRecognizer:tapGesture];
    
    
    // Check attendee only if not going to an event
    if (!self.event.isAttending) {
        [self.event isAttendeeWithCompletionHandler:^(BOOL attendee) {
            if (attendee == true) {
                self.event.isAttending = true;
                attending = attendee;
                if (eventDetails[@"Functions"] == nil) {
                    [eventDetails setObject:@[@"function_cells", @"function_cells"] forKey:@"Functions"];
                }
                [self.tableView reloadData];
            }
        }];
    }
    
    
}

- (void)goToProfile:(UITapGestureRecognizer *)tapGesture {
    NSLog(@"goToProfile");
    
    if (tapGesture.state == UIGestureRecognizerStateEnded)
        [self performSegueWithIdentifier:@"showProfileView" sender:self];
}

- (void)updateHeaderView {
    CGRect headerRect = CGRectMake(0, -(kEventTableHeaderHeight), self.tableView.bounds.size.width, kEventTableHeaderHeight);
    if (self.tableView.contentOffset.y < -kEventTableHeaderHeight) {
        headerRect.origin.y = self.tableView.contentOffset.y;
        headerRect.size.height = -self.tableView.contentOffset.y;
    }
    
    headerView.frame = headerRect;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self updateHeaderView];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[eventDetails allKeys] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sections = [eventDetails allKeys];
    return [[eventDetails objectForKey:[sections objectAtIndex:section]] count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return nil;
    }
    return [[eventDetails allKeys] objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 4) {
        return 100.0f;
    }
    
    return 71.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    UITableViewCell *cell;
    if (section == 0) {
        if (row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.textLabel.text = [NSString stringWithFormat:@"by %@", self.event.hosterInfo[@"fields"][@"full_name"]];
            //Checking cache for user image
            SAMCache *cache = [SAMCache sharedCache];
            NSString *userCacheKey = [NSString stringWithFormat:@"user_%@", self.event.hosterInfo[@"pk"]];
            if (![cache imageExistsForKey:userCacheKey]) {
                NSURL *profilePictureURL = [NSURL URLWithString:self.event.hosterInfo[@"fields"][@"profile_picture_url"]];
                //            cell.imageView.layer.borderWidth = 2.0f;
                //            cell.imageView.layer.borderColor = [UIColor colorWithRed:253.0f/255.0f green:216.0f/255.0f blue:117.0f/255.0f alpha:1.0f].CGColor;
                cell.imageView.clipsToBounds = YES;
                dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                dispatch_async(queue, ^{
                    NSData *imageData = [NSData dataWithContentsOfURL:profilePictureURL];
                    if (imageData != nil) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [cache setImage:[UIImage imageWithData:imageData] forKey:userCacheKey];
                            cell.imageView.image = [self imageWithImage:[UIImage imageWithData:imageData] convertToSize:CGSizeMake(40, 40)];
                        });
                    }
                });
            }
            else {
                cell.imageView.image = [self imageWithImage:[cache imageForKey:userCacheKey] convertToSize:CGSizeMake(40, 40)];
            }
            
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.frame = CGRectMake(0, 0, 42, 42);
            cell.detailTextLabel.hidden = YES;
        }
        else if (row == 1) {
            //Date time
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.textLabel.text = [self dateStringFromStartDate:self.event.startTimeStamp andEndDate:self.event.endTimeStamp];
            cell.imageView.image = [UIImage imageNamed:@"Calender"];
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                                fromDate:[NSDate date]
                                                                  toDate:self.event.startTimeStamp
                                                                 options:0];
            if ([components day] > 0) {
                cell.detailTextLabel.text = [NSString stringWithFormat:@"starts in %ld days", [components day]];
            }
            else if ([components day] < 0){
                cell.detailTextLabel.text = [NSString stringWithFormat:@"ended %ld days ago.", labs([components day])];
            }
            else {
                cell.detailTextLabel.text = @"Ongoing event";
            }
        }
        else if (row == 2) {
            //ticket price
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            if (self.event.price > 0.0)
                cell.textLabel.text = [NSString stringWithFormat:@"Ticket Price: $%.2lf", self.event.price];
            else
                cell.textLabel.text = [NSString stringWithFormat:@"Ticket Price: Free"];
            cell.imageView.image = [UIImage imageNamed:@"Dollar-1"];
            cell.detailTextLabel.text = @"";
        }
        else if (row == 3) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.textLabel.text = self.event.address;
            cell.imageView.image = [UIImage imageNamed:@"Location"];
            cell.detailTextLabel.text = @"";
        }
        else if (row == 4) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.textLabel.text = [NSString stringWithFormat:@"Attending: %ld", (long)self.event.attending];
            cell.imageView.image = [UIImage imageNamed:@"Attending"];
            cell.detailTextLabel.text = @"";
            
        }
        else if (row == 5) {
            EventFeedAttendCell *cell1 = [tableView dequeueReusableCellWithIdentifier:@"attend_button"];
            cell1.event = self.event;
            cell1.attending = attending;
            if (attending == true) {
                [cell1.attendButton setTitle:@"Cancel" forState:UIControlStateNormal];
                cell1.infoLabel.text = @"Clicking on cancel will initiate a refund process.";
            }
            else {
                [cell1.attendButton setTitle:@"Attend" forState:UIControlStateNormal];
                cell1.infoLabel.text = @"All payments are handled by Stripe Inc.";
            }
            return cell1;
        }
    }
    
    
    if (section == 1) {
        if (row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.textLabel.text = @"Chatroom";
            cell.imageView.image = [UIImage imageNamed:@"chat_bubble"];
            cell.detailTextLabel.text = @"";
        }
        else if (row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
            cell.textLabel.text = @"Checklists";
            cell.imageView.image = [UIImage imageNamed:@"Checklist"];
            cell.detailTextLabel.text = @"";
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIndentifier = [eventDetails[@"event_details"] objectAtIndex:indexPath.row];
    
    if ([cellIndentifier isEqualToString:@"attending"]) {
        [self performSegueWithIdentifier:@"showAttendeesView" sender:self];
    }
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        [self performSegueWithIdentifier:@"showChatView" sender:self];
    }
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        [self performSegueWithIdentifier:@"showChecklistView" sender:self];
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        cell.textLabel.text = @"Loading...";
        if (self.event.hosterInfo[@"pk"] != [[User sharedInstance] userId]) {
            ProfileViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
            viewController.profileState = ProfileStateOtherUser;
            viewController.segueClass = SenderSegueClassEventFeedViewController;
            viewController.tempUserDict = self.event.hosterInfo;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
    
    if (indexPath.section == 0 && indexPath.row == 3) {
        [self displayNavigationSettings];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


- (NSString *)dateStringFromStartDate:(NSDate * __nullable)startTimeStamp andEndDate:(NSDate * __nullable)endTimeStamp {
    NSString *dateString = nil;
    
    //NSLog(@"%@", [NSTimeZone systemTimeZone]);
    //NSLog(@"%@", self.event.startTimeStamp);
    NSDate *startInTZ = [self getCurrentDateTimeFromUTC:startTimeStamp];
    NSDate *endInTZ = [self getCurrentDateTimeFromUTC:endTimeStamp];
    
    NSInteger hours = [[[NSCalendar currentCalendar] components:NSCalendarUnitHour fromDate:startInTZ toDate:endInTZ options:0] hour];
    NSLog(@"%ld", hours);
    if (hours <= 24) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [timeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"EEEE, MMMM, d"];
        [timeFormatter setDateFormat:@"h:mm a"];
        dateString = [NSString stringWithFormat:@"%@ from %@ to %@", [dateFormatter stringFromDate:startInTZ], [timeFormatter stringFromDate:startInTZ], [timeFormatter stringFromDate:endInTZ]];
    }
    else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"E, MMM, d, y h:mm a"];
        dateString = [NSString stringWithFormat:@"%@ to\n%@", [dateFormatter stringFromDate:startInTZ], [dateFormatter stringFromDate:endInTZ]];
    }
    return dateString;
}

- (NSDate *)getCurrentDateTimeFromUTC:(NSDate *)utcDate {
    NSTimeInterval timeZoneSeconds = [[NSTimeZone localTimeZone] secondsFromGMT];
    NSDate *dateInLocalTimezone = [utcDate dateByAddingTimeInterval:timeZoneSeconds];
    return dateInLocalTimezone;
}

- (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

- (void)displayNavigationSettings {
    // Show alert controller with different types of map options
    UIAlertController *alertSheet = [UIAlertController alertControllerWithTitle:@"Open in" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertSheet addAction:[UIAlertAction actionWithTitle:@"Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *mapURL = [NSString stringWithFormat:@"http://maps.apple.com/?sll=%f,%f&z=2", self.event.latitude, self.event.longitude];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:mapURL] options:@{} completionHandler:nil];
    }]];
    
    [alertSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertSheet dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alertSheet animated:YES completion:nil];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showChatView"]) {
        ChatViewController *viewController = (ChatViewController *)segue.destinationViewController;
        viewController.event = self.event;
    }
    
    if ([segue.identifier isEqualToString:@"showChecklistView"]) {
        ChecklistViewController *viewController = (ChecklistViewController *)segue.destinationViewController;
        viewController.event = self.event;
    }
    if ([segue.identifier isEqualToString:@"showProfileView"]) {
        ProfileViewController *viewController = (ProfileViewController *)segue.destinationViewController;
        viewController.profileState = ProfileStateOtherUser;
        viewController.segueClass = SenderSegueClassEventFeedViewController;
    }
    if ([segue.identifier isEqualToString:@"showAttendeesView"]) {
        AttendeesViewController *viewController = (AttendeesViewController *)segue.destinationViewController;
        viewController.event = self.event;
    }
    
    if ([segue.identifier isEqualToString:@"showEventName"]) {
        EventNameViewController *viewController = (EventNameViewController *)segue.destinationViewController;
        viewController.event = self.event;
        viewController.eventState = EventUpdateState;
    }
}
@end
