//
//  Event.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "Event.h"

#pragma mark - Constant values
NSString *const kEventImageDataLoaded = @"event_image_data_loaded";
NSString *const kEventImageUploaded = @"event_image_uploaded";
NSString *const kEventDateTimeFormat = @"yyyy-MM-dd HH:mm:ss Z";

@implementation Attendee

+ (NSDictionary *)keyPathsByPropertyKey {
    return @{@"fullName": @"full_name",
             @"profilePictureURL": @"profile_picture_url",
             @"desp": @"description",
             @"profileLink": @"profile_link",
             @"score": @"score",
             @"gender": @"gender"
             };
}

+ (NSDictionary *)defaultPropertyValues {
    return @{@"fullName": @"User.FullName",
             @"profilePictureURL": @"http://1.bp.blogspot.com/-sP9gGlZ1lWc/TpH2yX9ZbhI/AAAAAAAAAIw/I7MuW4jnBmc/s1600/facebook-blank-face-blank.jpg",
             @"desp": @"",
             @"profileLink": @"",
             @"score": @(0),
             @"gender" : @"male"
             };
}

@end

@implementation Event
- (instancetype)init {
    self = [super init];
    if (self) {
        self.imageData = nil;
        self.chatNotifications = true;
        self.attending = 0;
        self.isAttending = FALSE;
        //[self getEventImageData];
    }
    
    return self;
}


- (void)getEventImageData{
    if (self.imageURL == nil)
    return;
    
    NSURL *imageURL = [NSURL URLWithString:self.imageURL];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        if (imageData != nil){
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageData = imageData;
            });
        }
    });
}


- (BOOL)hasDelegate {
    return self.delegate != nil;
}


- (void)uploadEventWithCompletionHandler:(void (^)(BOOL))completionHandler
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    self.hosterInfo = [NSDictionary dictionaryWithObjectsAndKeys:[[User sharedInstance] userId], @"id", nil];
    NSString *uploadURL = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/create_event/"];
    
    //Uploading Image
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *eventImagesRef, *currentImageRef;
    eventImagesRef = [[storage reference] child:@"event_images"];
    currentImageRef = [eventImagesRef child:[NSString stringWithFormat:@"%@.jpeg", [self getUniqueHash]]];
    
    [currentImageRef putData:self.imageData metadata:nil completion:^(FIRStorageMetadata * _Nullable metadata, NSError * _Nullable error) {
        if (error != nil){
            NSLog(@"Error");
            completionHandler(false);
        }
        else{
            //Upload data to server
            self.imageURL = metadata.downloadURL.absoluteString;
            NSLog(@"%@", [self requestPayload]);
            [manager POST:uploadURL parameters:[self requestPayload] progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"Uploaded. %@", responseObject);
                completionHandler(true);
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                NSLog(@"%@", task.response.description);
                completionHandler(false);
            }];
        }
    }];
}


- (NSDictionary *)requestPayload
{
    //date formatter in order to format date in python style
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    
    NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
    [temp setObject:(self.eventId != nil) ? self.eventId : @"" forKey:@"event_id"];
    [temp setObject:self.eventName forKey:@"event_name"];
    [temp setObject:(self.hosterInfo[@"id"] != nil) ? self.hosterInfo[@"id"] : self.hosterInfo[@"pk"] forKey:@"hoster_info"];
    [temp setObject:[formatter stringFromDate:self.startTimeStamp] forKey:@"start_timestamp"];
    [temp setObject:[formatter stringFromDate:self.endTimeStamp] forKey:@"end_timestamp"];
    [temp setObject:[NSString stringWithFormat:@"%.2f", self.price] forKey:@"price"];
    [temp setObject:self.currency forKey:@"currency"];
    [temp setObject:self.address forKey:@"address"];
    [temp setObject:[NSString stringWithFormat:@"%lf", self.latitude] forKey:@"latitude"];
    [temp setObject:[NSString stringWithFormat:@"%lf", self.longitude] forKey:@"longitude"];
    [temp setObject:[NSString stringWithFormat:@"%d", self.isPrivate] forKey:@"is_private"];
    [temp setObject:self.imageURL forKey:@"image_url"];
    return [NSDictionary dictionaryWithDictionary:temp];
}



- (void)isAttendeeWithCompletionHandler:(void(^)(BOOL attendee))completionHandler {
    NSDictionary *payload = @{@"event_id": self.eventId,
                              @"user_id": [[User sharedInstance] userId]};
    NSString *isAttendeeEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/events/is_attendee"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:isAttendeeEndpoint parameters:payload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        self.isAttending = TRUE;
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)[task response];
        if ([response statusCode] == 404) {
            self.isAttending = FALSE;
            completionHandler(false);
        }
    }];
}

- (NSString *)getUniqueHash{
    return [[NSUUID UUID] UUIDString];
}

- (void)increaseAttend {
    self.attending = self.attending + 1;
}

- (void)decreaseAttend {
    self.attending = self.attending - 1;
}

- (void)loadAttendees {
    NSDictionary *params = @{@"id": self.eventId};
    NSString *getAttendeesEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/events/list_attendees/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:getAttendeesEndpoint parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *attendees = [NSMutableArray array];
        for (NSDictionary *jsonData in responseObject) {
            Attendee *newAttendee = [MGObjectMapper modelOfClass:Attendee.class fromDictionary:jsonData[@"fields"]];
            newAttendee.userId = jsonData[@"pk"];
            [attendees addObject:newAttendee];
        }
        
        if ([self hasDelegate]) {
            [self.delegate loadedAttendees:attendees];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", task.response);
        if ([self hasDelegate]) {
            [self.delegate loadedAttendees:nil];
        }
    }];
}

- (void)updateEventWithCompletionHandler:(void(^)(BOOL success))completionHandler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *updateEventEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/update_event/"];
    [manager POST:updateEventEndpoint parameters:[self requestPayload] progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@\n%@", task.response, [error localizedDescription]);
        completionHandler(false);
    }];
}

- (void)deleteEventWithCompletionHandler:(void(^)(BOOL success))completionHandler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSString *deleteEventEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/delete_event/"];
    [manager POST:deleteEventEndpoint parameters:@{@"event_id": self.eventId} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(false);
    }];
}

#pragma mark - Class Methods

+ (void)getEventFeedWithCompletionHandler:(void (^)(BOOL, NSArray<Event *> *))completionHandler {
    NSDictionary *requestPayload = @{@"user_id": [[User sharedInstance] userId]};
    NSString *eventFeedURL = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/event_feed/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:eventFeedURL parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray <Event *> *events = [[NSMutableArray alloc] init];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:kEventDateTimeFormat];
        //NSLog(@"%@", responseObject);
        for (NSDictionary *event in responseObject) {
            Event *tempEvent = [[Event alloc] init];
            tempEvent.eventId = event[@"id"];
            tempEvent.eventName = event[@"event_name"];
            tempEvent.hosterInfo = event[@"hoster"];
            tempEvent.startTimeStamp = [formatter dateFromString:event[@"from_timestamp"]];
            tempEvent.endTimeStamp = [formatter dateFromString:event[@"to_timestamp"]];
            tempEvent.price = [event[@"price"] doubleValue];
            tempEvent.currency = event[@"currency"];
            tempEvent.address = event[@"address"];
            tempEvent.attending = (NSInteger *)[event[@"attending"] integerValue];
            tempEvent.latitude = [event[@"latitude"] doubleValue];
            tempEvent.longitude = [event[@"longitude"] doubleValue];
            tempEvent.createdAt = [formatter dateFromString:event[@"created_at"]];
            tempEvent.imageURL = event[@"event_image_url"];
            tempEvent.isPrivate = [event[@"is_private"] boolValue];
            tempEvent.imageData = nil;
            
            [events addObject:tempEvent];
        }
        
        completionHandler(true, events);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", task.response);
    }];
}
@end
