//
//  MyEventsViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/30/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Event.h"
#import "SWRevealViewController.h"
#import <AMSmoothAlert/AMSmoothAlertView.h>

@interface MyEventsViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *myEventControl;
- (IBAction)valueChanged:(id)sender;

@end
