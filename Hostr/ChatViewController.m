//
//  ChatViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/13/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "ChatViewController.h"
#import "URLBuilder.h"

@interface ChatViewController ()
@property (nonatomic, strong) NSMutableArray<JSQMessage *> *messages;
@property (nonatomic) FIRDatabaseHandle messageArriveHandle;

@end

@implementation ChatViewController {
    NSString *eventChatId;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.senderId = [NSString stringWithFormat:@"%@_user", [[User sharedInstance] userId]];
    self.senderDisplayName = [[User sharedInstance] fullName];
    
    self.ref = [[FIRDatabase database] reference];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    self.inputToolbar.contentView.leftBarButtonItem = nil;
    
    eventChatId = [NSString stringWithFormat:@"%@_messages", self.event.eventId];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newMessageArrived:) name:kNewGroupMessage object:nil];
    
//    if (self.messageArriveHandle == 0) {
//        FIRDatabaseReference *messagesRef = [self.ref child:eventChatId];
//        self.messageArriveHandle = [messagesRef observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
//            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
//            if (snapshot.value != [NSNull null]) {
//                if (self.senderId != snapshot.value[@"sender_id"]) {
//                    NSDate *timestamp = [dateFormatter dateFromString:snapshot.value[@"timestamp"]];
//                    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:snapshot.value[@"sender_id"] senderDisplayName:snapshot.value[@"display_name"] date:timestamp text:snapshot.value[@"text"]];
//                    [self.messages addObject:message];
//                }
//            }
//        }];
//    }
}

- (void)viewWillAppear:(BOOL)animated {
    //Removing any unnesscessary listenners for firebase
    [self.ref removeAllObservers];
    [super viewWillAppear:animated];
    self.messages = [[NSMutableArray alloc] init];
    [self syncMessagesFromServer];
    
    if (self.event.chatNotifications) {
        [self.muteNotificationsButton setTitle:@"Mute"];
    }
    else {
        [self.muteNotificationsButton setTitle:@"Unmute"];
    }
}

- (void)scrollToBottomAnimated:(BOOL)animated
{
    if ([self.collectionView numberOfSections] == 0) {
        return;
    }
    
    NSInteger items = [self.collectionView numberOfItemsInSection:0];
    
    if (items > 0) {
        [self.collectionView layoutIfNeeded];
        CGRect scrollRect = CGRectMake(0, self.collectionView.contentSize.height - 1.f, 1.f, 1.f);
        [self.collectionView scrollRectToVisible:scrollRect animated:animated];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSLog(@"removing all observers");
    //[self.ref removeAllObservers];
}

- (void) syncMessagesFromServer {
    // Sync all messages from Firebase server
    
    FIRDatabaseReference *messagesRef = [self.ref child:eventChatId];
    [messagesRef  observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSLog(@"%@", snapshot.value);
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        if (snapshot.value != [NSNull null]) {
            for (NSString *messageId in [snapshot.value allKeys].reverseObjectEnumerator) {
                NSDate *timeStamp = [dateFormatter dateFromString:snapshot.value[messageId][@"timestamp"]];
                JSQMessage *message = [[JSQMessage alloc] initWithSenderId:snapshot.value[messageId][@"sender_id"] senderDisplayName:snapshot.value[messageId][@"display_name"] date:timeStamp text:snapshot.value[messageId][@"text"]];
                [self.messages addObject:message];
                [self.messages sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                    NSDate *date1 = [(JSQMessage *)obj1 date];
                    NSDate *date2 = [(JSQMessage *)obj2 date];
                    
                    return [date1 compare:date2];
                }];
                [self.collectionView reloadData];
                [self scrollToBottomAnimated:YES];
            }
        }
    }];
    
    
}

- (void) sendMessage:(JSQMessage *)message {
    FIRDatabaseReference *messagesRef = [self.ref child:eventChatId];
    FIRDatabaseReference *newMessageRef = [messagesRef childByAutoId];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDictionary *messagePayload = @{@"sender_id": message.senderId,
                                     @"display_name": message.senderDisplayName,
                                     @"text": message.text,
                                     @"timestamp": [dateFormatter stringFromDate:message.date]
                                     };
    [newMessageRef updateChildValues:messagePayload];
}

- (void) sendChatNotification:(JSQMessage *)message {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDictionary *requestPayload = @{@"message_title": [NSString stringWithFormat:@"New message: ~%@", self.event.eventName],
                                     @"message_body": [NSString stringWithFormat:@"%@: %@", message.senderDisplayName, message.text],
                                     @"event_id": self.event.eventId,
                                     @"sender_id": message.senderId,
                                     @"sender_display_name": message.senderDisplayName,
                                     @"text": message.text,
                                     @"timestamp": [dateFormatter stringFromDate:message.date]};
    NSString *chatNotificationEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"POST" andWithEndpoint:@"/events/chat_notification/"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:chatNotificationEndpoint parameters:requestPayload progress:nil success:nil failure:nil];
}

- (void)newMessageArrived:(NSNotification *)notification {
    NSDictionary *userInfo = [notification userInfo];
    if ([userInfo[@"event_id"] integerValue] == [self.event.eventId integerValue]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *timestamp = [dateFormatter dateFromString:userInfo[@"timestamp"]];
        JSQMessage *newMessage = [[JSQMessage alloc] initWithSenderId:userInfo[@"sender_id"] senderDisplayName:userInfo[@"sender_display_name"] date:timestamp text:userInfo[@"text"]];
        [self.messages addObject:newMessage];
        [self.messages sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            NSDate *date1 = [(JSQMessage *)obj1 date];
            NSDate *date2 = [(JSQMessage *)obj2 date];
            
            return [date1 compare:date2];
        }];
        [self.collectionView reloadData];
        [self scrollToBottomAnimated:YES];
    }
}

#pragma mark - JSQ Delegate Methods
- (void)didPressSendButton:(UIButton *)button withMessageText:(NSString *)text senderId:(NSString *)senderId senderDisplayName:(NSString *)senderDisplayName date:(NSDate *)date {
    NSLog(@"didPressSend");
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId senderDisplayName:self.senderDisplayName date:[NSDate date] text:text];
    [self.messages addObject:message];
    [self.collectionView reloadData];
    [self scrollToBottomAnimated:YES];
    [self sendMessage:message];
    
    //Sending notification
    [self sendChatNotification:message];
    
    [self finishSendingMessageAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.messages.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    JSQMessage *message;
    if (self.messages.count > 0) {
        message = [self.messages objectAtIndex:indexPath.row];
    }
    else {
        return nil;
    }
    if (message.senderId != self.senderId) {
        cell.textView.textColor = [UIColor blackColor];
    }
    return cell;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message, *previousMessage;
    message = [self.messages objectAtIndex:indexPath.row];
    if (indexPath.row != 0) {
        previousMessage = [self.messages objectAtIndex:(indexPath.row - 1)];
        return (message.senderId != previousMessage.senderId) ? 15.0f : 0.0f;
    }
    return 15.0f;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessage *message = [self.messages objectAtIndex:indexPath.row];
    if (indexPath.row != 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:(indexPath.row - 1)];
        return ([message.senderId isEqualToString:previousMessage.senderId]) ? nil : [[NSAttributedString alloc] initWithString:message.senderDisplayName];
    }
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:message.senderDisplayName];
    return attributedString;
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [self.messages objectAtIndex:indexPath.row];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    JSQMessage *message = [self.messages objectAtIndex:indexPath.row];
    if (self.senderId == message.senderId) {
        return [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0f]];
//        return [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor colorWithRed:255.0f/255.0f green:96.0f/255.0f blue:70.0f/255.0f alpha:0.8f]];
    }
    
     return [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1.0f]];
//    return [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed:79.0f/255.0f green:229.0f/255.0f blue:255.0f/255.0f alpha:1.0f]];
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}
- (IBAction)muteNotifications:(id)sender {
    //Mute chat notifications
    NSDictionary *requestPayload = @{@"event_id": self.event.eventId,
                                     @"user_id": [[User sharedInstance] userId],
                                     @"notif_flag": (self.event.chatNotifications) ? @false : @true};
    NSString *editChatNotificationEndpoint = [[URLBuilder sharedInstance] urlWithRequestType:@"GET" andWithEndpoint:@"/events/edit_chat_notifications"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:editChatNotificationEndpoint parameters:requestPayload progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (self.event.chatNotifications == true) {
            self.event.chatNotifications = false;
            [self.muteNotificationsButton setTitle:@"Unmute"];
        }
        
        else if (self.event.chatNotifications == false) {
            self.event.chatNotifications = true;
            [self.muteNotificationsButton setTitle:@"Mute"];
        }
    } failure:nil];
}


- (void)messageTextChaged {
    NSLog(@"messageTextChanged");
}
@end
