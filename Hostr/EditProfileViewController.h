//
//  EditProfileViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/7/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XLForm/XLForm.h>
#import <AMSmoothAlert/AMSmoothAlertView.h>
#import "User.h"

@interface EditProfileViewController : XLFormViewController
- (IBAction)saveProfileInfo:(id)sender;

@end
