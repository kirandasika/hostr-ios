//
//  EventLocationViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/5/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <XLForm/XLForm.h>
#import <AFNetworking/AFNetworking.h>

@interface EventLocationViewController : UIViewController<XLFormRowDescriptorViewController>
@property (nonatomic, weak) IBOutlet UITextView *addressTextView;
@end
