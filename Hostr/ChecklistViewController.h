//
//  ChecklistViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/16/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "User.h"
#import "URLBuilder.h"
#import <AFNetworking/AFNetworking.h>
#import "CheckListItem.h"

@interface ChecklistViewController : UITableViewController<CheckListItemDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addItemButton;
- (IBAction)addItem:(id)sender;

@property (nonatomic, strong) Event *event;
@end

@interface Item : NSObject
@property (nonatomic, strong) NSString *itemId;
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic) BOOL isCompleted;
@property (nonatomic, strong) NSDate *createdAt;
@end
