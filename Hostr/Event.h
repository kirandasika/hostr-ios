//
//  Event.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import <AFNetworking/AFNetworking.h>
#import "URLBuilder.h"
#import <MGObjectMapper/MGObjectMapper.h>
@import Firebase;
@protocol EventEntity <NSObject>

@required
- (void)loadedAttendees:(NSArray *)attendees;

@end
@interface Event : NSObject
@property (nonatomic, strong) NSString *eventId;
@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSDictionary *hosterInfo;
@property (nonatomic, strong) NSDate *startTimeStamp;
@property (nonatomic, strong) NSDate *endTimeStamp;
@property (nonatomic) double price;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, assign) NSUInteger attending;
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;
@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic) BOOL chatNotifications;
@property (nonatomic) BOOL isAttending;
@property (nonatomic) BOOL isPrivate;
@property (nonatomic, weak) id<EventEntity> delegate;

#pragma mark - Constants
extern NSString *const kEventImageDataLoaded;
extern NSString *const kEventDateTimeFormat;

#pragma mark - Methods
- (instancetype)init;
- (void)decreaseAttend;
- (void)increaseAttend;
- (void)getEventImageData;
- (void)uploadEventWithCompletionHandler:(void(^)(BOOL success))completionHandler;
- (void)updateEventWithCompletionHandler:(void(^)(BOOL success))completionHandler;
- (void)deleteEventWithCompletionHandler:(void(^)(BOOL success))completionHandler;
- (void)isAttendeeWithCompletionHandler:(void(^)(BOOL attendee))completionHandler;
- (void)loadAttendees;
+ (void)getEventFeedWithCompletionHandler:(void(^)(BOOL success, NSArray <Event *> *events))completionHandler;
@end


@interface Attendee : NSObject<MGObjectMapperResource>
@property (nonatomic) NSString *userId;
@property (nonatomic) NSString *fullName;
@property (nonatomic) NSString *profilePictureURL;
@property (nonatomic) NSString *desp;
@property (nonatomic) NSString *profileLink;
@property (nonatomic) NSString *gender;
@property (nonatomic) NSInteger score;
@end
