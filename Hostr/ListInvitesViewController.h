//
//  ListInvitesViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 8/4/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "User.h"
#import "URLBuilder.h"
#import <AFNetworking/AFNetworking.h>

@interface ListInvitesViewController : UITableViewController
@property (nonatomic, strong) Event *event;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButtonItem;

-(IBAction)inviteConnections:(id)sender;
@end
