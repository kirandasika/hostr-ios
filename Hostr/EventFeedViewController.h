//
//  EventFeedViewController.h
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/8/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
@interface EventFeedViewController : UITableViewController<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *headerImageView;
@property (nonatomic, strong) Event *event;
@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profilePictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIView *placeHolderView;

extern NSString *const kAttendStateChanged;

@end


@interface EventFeedAttendCell : UITableViewCell
@property (nonatomic, strong) Event *event;
@property (weak, nonatomic) IBOutlet UILabel *  infoLabel;
@property (weak, nonatomic) IBOutlet UIButton *  attendButton;
@property (nonatomic) BOOL attending;
@end
