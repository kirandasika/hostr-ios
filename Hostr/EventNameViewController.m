//
//  EventNameViewController.m
//  Hostr
//
//  Created by Sai Kiran Dasika on 7/3/17.
//  Copyright © 2017 Sai Kiran Dasika. All rights reserved.
//

#import "EventNameViewController.h"
#import "EventDetailsViewController.h"

@interface EventNameViewController ()
@end

@implementation EventNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.eventNameTextField.delegate = self;
    if (self.eventState == EventCreationState) {
        self.event = [[Event alloc] init];
    }
    else if (self.eventState == EventUpdateState) {
        self.eventNameTextField.text = self.event.eventName;
    }
}

- (void)viewWillAppear:(BOOL)animated{
    if (![self.continueButton isEnabled])
        [self.continueButton setEnabled:YES];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:233.0f/255.0f green:84.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.translucent = NO;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.eventNameTextField becomeFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.eventNameTextField resignFirstResponder];
    NSLog(@"Event name: %@", self.eventNameTextField.text);
    if ([self.eventNameTextField.text length] != 0) {
        self.event.eventName = self.eventNameTextField.text;
    }
    [self performSegueWithIdentifier:@"showEnterEventDetails" sender:self];
    return NO;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showEnterEventDetails"]) {
        EventDetailsViewController *viewController = (EventDetailsViewController *)segue.destinationViewController;
        viewController.event = self.event;
        
        if (self.eventState == EventUpdateState) {
            viewController.eventState = EventUpdateState;
        }
    }
}

- (IBAction)eventDetails:(id)sender {
    [self.continueButton setEnabled:NO];
    self.event.eventName = self.eventNameTextField.text;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^a-zA-Z0-9]" options:NSRegularExpressionCaseInsensitive error:nil];
    if ([regex firstMatchInString:self.event.eventName options:NSMatchingReportCompletion range:NSRangeFromString(self.event.eventName)] != nil || self.event.eventName.length < 4) {
        UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Error" message:@"The event name must be more than 4 characters or there are invalid characters present such as emojis." preferredStyle:UIAlertControllerStyleAlert];
        [alertView addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [alertView dismissViewControllerAnimated:YES completion:nil];
        }]];
        
        [self presentViewController:alertView animated:YES completion:nil];
    }
    else
        [self performSegueWithIdentifier:@"showEnterEventDetails" sender:self];
}
@end
