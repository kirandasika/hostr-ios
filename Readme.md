# Hostr

Welcome to Hostr-iOS repository. This repository is maintained by [Sai Kiran Dasika](mailto://kirandasika30@gmail.com). Feel free to contact me if you have any questions. 

In order to run the app properly. Follow the steps to install and run the app on your machine.

Step 1: Change directory into the app .xcworkspace file

Step 2: 

```
pod install
```
This step will install all dependencies that is required by the application.

Step 3: Run the app through Xcode and enjoy :)
